<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert alert-danger my-alert" role="alert">
	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	<span class="sr-only">Error:</span>User name '<c:out value="${sessionScope.error}"/>' doesn't exist or password is wrong.
</div> 