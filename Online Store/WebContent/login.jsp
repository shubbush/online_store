<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp"/>
  <div id="form-div">
    <form class="form" action="login" method="post">
      <c:if test ="${sessionScope.error != null}">
	<jsp:include page="error.jsp"/>
	</c:if>
      <p class="name">
        <input name="user" type="text" class="feedback-input" placeholder="Name" id="name" />
      </p>
      <p class="password">
        <input name="pwd" type="password" class="feedback-input" id="password" placeholder="Password" />
      </p>
      <div class="submit">
        <input type="submit" value="Login" id="button-blue"/>
      </div>
      
      <div class="submit">
      	<input type="button" value="Sign Up" id="button-blue"/>
      </div>
    </form>
  </div>
<jsp:include page="footer.jsp"/>