package ru.lanit.shubanev.controller;

public class FrontController {

	/** The contr. */
	private Controller contr;

	/** The user controller. */
	private UserController userController;

	/** The storage controller. */
	private StorageController storageController;

	/** The product controller. */
	private ProductController productController;

	/** The deliveryController. */
	private DeliveryController deliveryController;

	private AccessController accessController;

	/**
	 * @param contr
	 * @param userController
	 * @param storageController
	 * @param productController
	 * @param deliveryController
	 * @param accessController
	 */
	public FrontController() {
		this.contr = new Controller();
		this.userController = new UserController();
		this.storageController = new StorageController();
		this.productController = new ProductController();
		this.deliveryController = new DeliveryController();
		this.accessController = new AccessController();
	}

	/**
	 * @return the contr
	 */
	public Controller getContr() {
		return contr;
	}

	/**
	 * @return the userController
	 */
	public UserController getUserController() {
		return userController;
	}

	/**
	 * @return the storageController
	 */
	public StorageController getStorageController() {
		return storageController;
	}

	/**
	 * @return the productController
	 */
	public ProductController getProductController() {
		return productController;
	}

	/**
	 * @return the deliveryController
	 */
	public DeliveryController getDeliveryController() {
		return deliveryController;
	}

	/**
	 * @return the accessController
	 */
	public AccessController getAccessController() {
		return accessController;
	}

}
