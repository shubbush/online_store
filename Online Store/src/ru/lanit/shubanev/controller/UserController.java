package ru.lanit.shubanev.controller;

import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.user.Cart;
import ru.lanit.shubanev.model.user.User;

/**
 * The Class UserController.
 */
public class UserController {

	/** The user dao. */
	private UserDAO userDao;

	/**
	 * Instantiates a new user controller.
	 */
	public UserController() {
		super();
		AbstractDAOFactory dao = AbstractDAOFactory.getDAOFactory();
		this.userDao = dao.getUserDAO();
	}

	/**
	 * Adds the carts.
	 *
	 * @param carts
	 *            the carts
	 */
	public void addCart(Cart cart) {
		userDao.addCart(cart);
	}

	/**
	 * Adds the product to user cart.
	 *
	 * @param productId
	 *            the prod
	 * @param quantity
	 *            the quantity
	 * @param user
	 *            the user
	 */
	public void addProductToUserCart(long productId, int quantity, long userId) {
		DeliveryController deliveryController = new DeliveryController();
		List<Logistic> tmpLog = deliveryController.getUserLogistic(productId,
				quantity);
		userDao.addProductToUserCart(tmpLog, userId);
	}

	/**
	 * Add staff.
	 *
	 * @param users
	 *            the users
	 */
	public void addUser(String login, String password) {
		userDao.addUser(login, Controller.getMD5(password));
	}

	public User getUserByLogin(String login) {
		return userDao.getUserByLogin(login);
	}

	/**
	 * Gets the cart.
	 *
	 * @param userId
	 *            the user id
	 * @return the cart
	 */
	public Cart getCart(long userId) {
		return userDao.getUserCart(userId);
	}

	/**
	 * Вывод в консоль информации о пользователе.
	 *
	 * @param login
	 *            login
	 * @param password
	 *            password
	 * @return User
	 *
	 * @throws UserNotFoundException
	 *             the user not found exception
	 */

	public User signIn(String login, String password)
			throws UserNotFoundException {
		return userDao.signIn(login, password);
	}

	/**
	 * Get list of carts.
	 *
	 * @return the carts
	 */
	// public List<Cart> getCarts() {
	// return (List<Cart>) userDao.getCarts();
	// }

}
