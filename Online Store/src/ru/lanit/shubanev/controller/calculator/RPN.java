package ru.lanit.shubanev.controller.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.exceptions.WrongInputFormatException;
import ru.lanit.shubanev.exceptions.WrongNumberOfBracketsException;

/**
 * The Class RPN. Make Reverse Polish notation.
 */
public class RPN {

	/** The current storage. */
	private ProductDAO currentStorage;
	/** The Constant OPERATORSSECOND. */
	private static final String OPERATORSSECOND = "^*/";

	/** The Constant OPERATORS. */
	private static final String OPERATORS = "+-*/^%";

	/** The Constant DELIMS. */
	private static final String DELIMS = "+-*/^()%";
	/** The Constant REGEXOPERATORS. */
	private static final String REGEXOPERATORS = "[+-/*//^%]";

	/**
	 * Instantiates a new rpn.
	 *
	 */
	public RPN() {
		AbstractDAOFactory factory = AbstractDAOFactory.getDAOFactory();
		ProductDAO dao = factory.getProductDAO();
		this.currentStorage = dao;
	}

	/**
	 * Gets the rpn.
	 *
	 * @param request
	 *            the request
	 * @return the rpn
	 * @throws ProductNotFoundException
	 *             the product id not found exception
	 * @throws WrongNumberOfBracketsException
	 *             the wrong number of brackets exception
	 * @throws WrongInputFormatException
	 *             the wrong input format exception
	 */
	private List<String> getRPN(String request)
			throws ProductNotFoundException, WrongNumberOfBracketsException,
			WrongInputFormatException {
		List<String> result = Parser.checkString(request);
		List<String> rpn = new ArrayList<String>();
		Stack<String> operators = new Stack<String>();
		for (String str : result) {
			if (DELIMS.indexOf(str) == -1) {
				double tmp = currentStorage.getPriceByCode(str);
				if (tmp != -1) {
					rpn.add(Double.toString(tmp));
				} else {
					if (str.matches("[0-9]+.?[0-9]*")) {
						rpn.add(str);
					} else
						throw new ProductNotFoundException(str);
				}
			} else {
				checkOperator(str, rpn, operators);
			}
		}
		while (!operators.empty()) {
			rpn.add(operators.pop());
		}
		System.out.println("RPN generated...");
		return rpn;
	}

	/**
	 * Check operator.
	 *
	 * @param opr
	 *            the mathematical operator
	 * @param result
	 *            the current result
	 * @param operators
	 *            list of mathematical operators
	 */
	private void checkOperator(String opr, List<String> result,
			Stack<String> operators) {
		switch (opr) {
		case "^": {
			operators.push(opr);
			break;
		}
		case "(": {
			operators.push(opr);
			break;
		}
		case ")": {
			while (!operators.empty()
					&& !operators.peek().equalsIgnoreCase("(")) {
				result.add(operators.pop());
			}
			operators.pop();
			break;
		}
		case "+":
		case "-": {
			while (!operators.empty()
					&& OPERATORS.indexOf(operators.peek()) != -1) {
				result.add(operators.pop());
			}
			operators.push(opr);
			break;
		}
		case "*":
		case "/":
		case "%": {
			while (!operators.empty()
					&& OPERATORSSECOND.indexOf(operators.peek()) != -1) {
				result.add(operators.pop());
			}
			operators.push(opr);
			break;
		}
		}
	}

	/**
	 * Parse input string, delete spaces and Punctuation symbols, checked for
	 * unpair brackets, use Shunting-yard algorithm for create Reverse Polish
	 * notation and then get the result.
	 *
	 * @param request
	 *            the request
	 * @return the double result of input request
	 * @throws ProductNotFoundException
	 *             the book id not found exception
	 * @throws WrongNumberOfBracketsException
	 *             the wrong number of brackets exception
	 * @throws WrongInputFormatException
	 *             the wrong input format exception
	 */
	public double completeRPN(String request)
			throws ProductNotFoundException, WrongNumberOfBracketsException,
			WrongInputFormatException {
		List<String> rpn = getRPN(request);
		String element = "";
		for (int i = 0; i < rpn.size(); i++) {
			if (rpn.get(i).matches(REGEXOPERATORS)) {
				element = Double.toString(doOperation(
						Double.parseDouble(rpn.get(i - 2)),
						Double.parseDouble(rpn.get(i - 1)), rpn.get(i)));
				rpn.set(i, element);
				rpn.remove(i - 1);
				rpn.remove(i = i - 2);
			}
		}
		return Double.parseDouble(rpn.get(0));
	}

	/**
	 * Do operation.
	 *
	 * @param operand1
	 *            the operand1
	 * @param operand2
	 *            the operand2
	 * @param opr
	 *            the mathematical operator
	 * @return the double result of request
	 */
	private double doOperation(double operand1, double operand2, String opr) {
		switch (opr) {
		case "+": {
			return operand1 + operand2;
		}
		case "-": {
			return operand1 - operand2;
		}
		case "*": {
			return operand1 * operand2;
		}
		case "/": {
			return operand1 / operand2;
		}
		case "%": {
			return operand1 % operand2;
		}
		case "^": {
			return Math.pow(operand1, operand2);
		}
		default:
			return 0;
		}

	}
}
