package ru.lanit.shubanev.controller.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import ru.lanit.shubanev.exceptions.WrongInputFormatException;
import ru.lanit.shubanev.exceptions.WrongNumberOfBracketsException;

/**
 * The Class Parser. Parse the input string
 */
public class Parser {

	/** The Constant REGEXOPERATORS. */
	private static final String REGEXOPERATORS = "[+-/*//^%]";

	/** The Constant DELIMS. */
	private static final String DELIMS = "+-*/^()%";

	/** The Constant DELIMSWITHOUTBRACKET. */
	private static final String DELIMSWITHOUTBRACKET = "+-*/^%";

	/** The Constant REGEX. */
	private static final String REGEX = "[^a-zA-Z0-9-+*/&%^/t().]";

	/**
	 * Del spaces.
	 *
	 * @param request
	 *            the request
	 * @return the array list
	 */
	private static List<String> delSpaces(String request) {
		List<String> result = new ArrayList<String>();
		StringTokenizer str = new StringTokenizer(
				request.replaceAll(REGEX, ""), DELIMS, true);
		while (str.hasMoreTokens()) {
			result.add(str.nextToken());
		}
		// for(String st : result) {
		// System.out.println(st);
		// }
		System.out.println("Spaces deleted...");
		return result;
	}

	/**
	 * Check string.
	 *
	 * @param request
	 *            the request
	 * @return the array list
	 * @throws WrongNumberOfBracketsException
	 *             the wrong number of brackets exception
	 * @throws WrongInputFormatException
	 *             the wrong input format exception
	 */
	public static List<String> checkString(String request)
			throws WrongNumberOfBracketsException, WrongInputFormatException {
		int bracketCount = 0;
		boolean checkPrefer = false;
		List<String> result = delSpaces(request);
		if (result.size() != 0
				&& DELIMSWITHOUTBRACKET.indexOf(result.get(0)) == -1
				&& DELIMSWITHOUTBRACKET.indexOf(result.get(result.size() - 1)) == -1) {
			for (String tmp : result) {
				if (bracketCount < 0)
					throw new WrongNumberOfBracketsException();
				switch (tmp) {
				case "(":
					bracketCount++;
					break;
				case ")":
					if (!checkPrefer) {
						throw new WrongInputFormatException();
					}
					bracketCount--;
					break;
				default: {
					if (tmp.matches(REGEXOPERATORS)) {
						if (checkPrefer) {
							checkPrefer = false;
						} else {
							throw new WrongInputFormatException();
						}
					} else {
						if (!checkPrefer) {
							checkPrefer = true;
						} else {
							throw new WrongInputFormatException();
						}
					}
				}
				}
			}
			if (bracketCount == 0) {
				System.out.println("No input mistakes...");
				return result;
			}
		}
		throw new WrongInputFormatException();
	}
}
