package ru.lanit.shubanev.controller;

import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Storage;

/**
 * The Class StorageController.
 */
public class StorageController {

	/** The storage dao. */
	private StorageDao storageDao;

	/**
	 * Instantiates a new storage controller.
	 */
	public StorageController() {
		AbstractDAOFactory dao = AbstractDAOFactory.getDAOFactory();
		this.storageDao = dao.getStorageDao();
	}

	public void addLogistic(long productId, long storageId, int quantity) {
		storageDao.addLogistic(productId, storageId, quantity);
	}

	/**
	 * Add storage.
	 *
	 * @param storages
	 *            the storages
	 */
	public void addStorage(String title, int deliveryTime) {
		storageDao.addStorage(title, deliveryTime);
	}

	public int getDeliveryTimeByStorageId(long storageId)
			throws StorageNotFoundException {
		return storageDao.getDeliveryTimeById(storageId);
	}

	public long getStorageIdByTitle(String title) {
		return storageDao.getStorageIdByTitle(title);
	}

	/**
	 * Gets the product count.
	 *
	 * @param prod
	 *            the product
	 * @return the product count
	 */
	public int getProductCount(long productId) {
		return storageDao.getProductCount(productId);
	}

	public List<Storage> getStorages() {
		return storageDao.getStorages();
	}

	public void deleteProductLogistics(long productId) {
		storageDao.deleteProductLogistics(productId);
	}

}
