package ru.lanit.shubanev.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.PagedDao;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.model.ListItem;

public class PagedController<T extends ListItem> {

	private Class<T> classType;

	public PagedController(Class<T> classType) {
		this.classType = classType;
	}

	public List<T> getPage(int elementNumber, int count,
			Map<SortFilter, Object> map) throws ItemNotFoundException {

		List<T> pageList = null;
		AbstractDAOFactory factory = AbstractDAOFactory.getDAOFactory();
		PagedDao<T> dao = factory.getPagedDao(classType);
		pageList = Arrays.asList(dao.getPage(elementNumber, count, map));
		return pageList;
	}

	public int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		AbstractDAOFactory factory = AbstractDAOFactory.getDAOFactory();
		PagedDao<T> dao = factory.getPagedDao(classType);
		return dao.getItemsCountForRequest(map);

	}
}
