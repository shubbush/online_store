package ru.lanit.shubanev.controller;

import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.AccessDao;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.model.access.RolePermission;
import ru.lanit.shubanev.model.access.UserRole;
import ru.lanit.shubanev.model.user.User;

public class AccessController {

	private AccessDao accessDao;

	public AccessController() {
		AbstractDAOFactory dao = AbstractDAOFactory.getDAOFactory();
		this.accessDao = dao.getAccessDao();
	}

	public boolean haveUserPermission(String permission, User user) {
		if (permission == null) {
			return true;
		} else {
			if (user == null) {
				return false;
			}
			List<Long> rolesId = getUserRoles(user);
			List<Long> userPermissions = new ArrayList<>();
			List<String> stringUserPermission = new ArrayList<>();
			for (Long roleId : rolesId) {
				userPermissions.addAll(accessDao.getRolePemissions(roleId));
			}
			for (Long permis : userPermissions) {
				stringUserPermission.add(accessDao
						.getPermissionStringById(permis));
			}
			return stringUserPermission.contains(permission);
		}
	}

	public List<Long> getUserRoles(User user) {
		return accessDao.getUserRoles(user.getId());
	}

	public void createPermission(String title) {
		accessDao.addPermission(title);
	}

	public void createRole(String title) {
		accessDao.addRole(title);
	}

	public void createUserRole(String login, String title) {
		AbstractDAOFactory daoFactory = AbstractDAOFactory.getDAOFactory();
		UserDAO userDao = daoFactory.getUserDAO();
		long userId = userDao.getUserIdByLogin(login);
		long roleId = accessDao.getRoleByTitle(title).getId();
		accessDao.addUserRole(new UserRole(userId, roleId));
	}

	public void createRolePermission(String roleTitle, String permissionTitle) {
		long roleId = accessDao.getRoleByTitle(roleTitle).getId();
		long permissionId = accessDao.getPermissionByTitle(permissionTitle)
				.getId();
		accessDao.addRolePermission(new RolePermission(roleId, permissionId));
	}

	public List<String> getStringUserRoles(User user) {
		List<Long> rolesId = getUserRoles(user);
		List<String> rolesString = new ArrayList<>();
		for (Long roleId : rolesId) {
			rolesString.add(accessDao.getRoleStringById(roleId));
		}
		return rolesString;
	}
}
