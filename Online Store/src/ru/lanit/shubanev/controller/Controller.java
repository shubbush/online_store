package ru.lanit.shubanev.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.user.Cart;

/**
 * Используется для работы с хранилещем книг и пользователей.
 *
 * @author shubbush
 */
public class Controller {

	/**
	 * Инициализирует поле.
	 */
	public Controller() {
	}

	/**
	 * Метод создания MD5 хэша входной строки.
	 *
	 * @param input
	 *            String for hash.
	 * @return String Hash of input string
	 */
	public static String getMD5(String input) {
		final int numSystem = 16;
		final int strLength = 32;
		MessageDigest messageDigest = null;
		byte[] digest = new byte[0];

		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(input.getBytes());
			digest = messageDigest.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		BigInteger bigInt = new BigInteger(1, digest);
		String md5hex = bigInt.toString(numSystem);
		while (md5hex.length() < strLength) {
			md5hex = "0" + md5hex;
		}
		return md5hex;
	}

	/**
	 * Checkout and return deliveryController date.
	 *
	 * @param cart
	 *            the cart
	 * @return date
	 * @throws ProductQuantityInsufficientlyException
	 *             the product quantity insufficiently exception
	 * @throws StorageNotFoundException
	 */
	public Calendar checkout(Cart cart)
			throws ProductQuantityInsufficientlyException,
			StorageNotFoundException {
		DeliveryController deliveryController = new DeliveryController();
		deliveryController.checkoutCart(cart, cart.getUserId());
		return deliveryController.calculateDeliveryTime(cart.getCart());
	}

}
