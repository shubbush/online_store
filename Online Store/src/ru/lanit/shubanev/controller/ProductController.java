package ru.lanit.shubanev.controller;

import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.product.Spec;

/**
 * The Class ProductController.
 */
public class ProductController {

	private ProductDAO productDao;

	/**
	 * Constructor
	 */

	public ProductController() {
		AbstractDAOFactory dao = AbstractDAOFactory.getDAOFactory();
		this.productDao = dao.getProductDAO();

	}

	public long getProductIdByTitle(String title)
			throws ProductNotFoundException {
		return productDao.getProductIdByTitle(title);
	}

	public long getCategoryIdByTitle(String title) {
		return productDao.getCategoryId(title);
	}

	/**
	 * Adds the categorys.
	 *
	 * @param categs
	 *            the categs
	 */
	public void addCategory(String title, long parentId) {
		productDao.addCategory(title, parentId);
	}

	public void addProduct(String title, double price, String desc, String code) {
		productDao.addProduct(title, price, desc, code);
	}

	/**
	 * Add specification.
	 *
	 * @param specs
	 *            the specs
	 */
	public void addSpecification(long categoryId, String title) {
		productDao.addSpec(categoryId, title);
	}

	/**
	 * Gets the product by code.
	 *
	 * @param code
	 *            the code
	 * @return the product by code
	 */
	public Product getProductByCode(String code) {
		return productDao.getProductByCode(code);
	}

	public void addProductSpec(long productId, long specId, String value) {
		productDao.addProductSpec(productId, specId, value);
	}

	/**
	 * Gets the categories string.
	 *
	 * @return the categories string
	 */
	public String getCategoriesString() {
		return productDao.categoriesToString();
	}

	public List<Spec> getSpecsByCategoryId(long id) {
		return productDao.getCategorySpecifications(id);
	}

	public long getSpecIdByTitle(String title) {
		return productDao.getSpecIdByTitle(title);
	}

	public Product getProductById(long productId)
			throws ProductNotFoundException {
		return productDao.getProductById(productId);
	}

	public void deleteProduct(long productId) throws ProductNotFoundException {
		productDao.deleteProduct(productId);
	}

}
