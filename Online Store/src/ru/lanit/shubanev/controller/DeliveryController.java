package ru.lanit.shubanev.controller;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.user.Cart;

/**
 * The Class DeliveryCalculation. Calculate delivery time for users cart.
 */
public class DeliveryController {

	/** The Constant STANDART_DELIVERY_TIME. */
	private final static int STANDART_DELIVERY_TIME = 1;

	/** The Constant END_OF_WORKING_DAY. */
	private final static int END_OF_WORKING_DAY = 15;

	/**
	 * Calculate delivery time.
	 *
	 * @param cart
	 *            the cart of current user
	 * @return the calendar Calendar with the date of delivery
	 * @throws StorageNotFoundException
	 */

	public Calendar calculateDeliveryTime(List<Logistic> cart)
			throws StorageNotFoundException {
		StorageController storageController = new StorageController();
		Calendar cal = new GregorianCalendar();
		int deliverTime = 0;
		int currentTime = cal.get(Calendar.HOUR_OF_DAY);
		for (Logistic prodinfo : cart) {
			int tmp = storageController.getDeliveryTimeByStorageId(prodinfo
					.getStorageId());
			// storage.getDeliveryTime(prodinfo.getProduct().getId(),
			// prodinfo.getQuantity());
			if (tmp > deliverTime) {
				deliverTime = tmp;
			}
		}
		if (currentTime > END_OF_WORKING_DAY) {
			deliverTime++;
		}
		cal.add(Calendar.DAY_OF_MONTH, deliverTime + STANDART_DELIVERY_TIME);
		return cal;
	}

	/**
	 * Checkout. Get products from storage.
	 *
	 * @param cart
	 *            the cart of current user
	 * @throws ProductQuantityInsufficientlyException
	 *             the product quantity insufficiently exception
	 */
	public void checkoutCart(Cart cart, long userId)
			throws ProductQuantityInsufficientlyException {
		AbstractDAOFactory daoFactory = AbstractDAOFactory.getDAOFactory();
		StorageDao dao = daoFactory.getStorageDao();

		for (Logistic log : cart.getCart()) {
			dao.checkoutProduct(log, userId);
		}
	}

	/**
	 * Return list of logistics with users product from cart and storages.
	 *
	 * @param productId
	 *            the product id that adds to user cart
	 * @param quantity
	 *            the quantity of product
	 * @return the user logistic
	 */
	public List<Logistic> getUserLogistic(long productId, int quantity) {
		AbstractDAOFactory daoFactory = AbstractDAOFactory.getDAOFactory();
		StorageDao dao = daoFactory.getStorageDao();
		return dao.getUserLogistic(productId, quantity);
	}

}
