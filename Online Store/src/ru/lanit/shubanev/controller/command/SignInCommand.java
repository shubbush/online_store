package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.Controller;
import ru.lanit.shubanev.controller.UserController;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.user.User;

public class SignInCommand extends Command {

	private String login;
	private String pwd;

	public SignInCommand() {
		permission = null;
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws UserNotFoundException {
		login = (String) map.get(ExecuteArgument.login);
		pwd = (String) map.get(ExecuteArgument.password);
		UserController userController = new UserController();
		User user = userController.signIn(login, Controller.getMD5(pwd));
		if (user != null)
			map.put(ExecuteArgument.result, user);
	}
}
