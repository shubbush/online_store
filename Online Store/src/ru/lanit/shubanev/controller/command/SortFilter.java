package ru.lanit.shubanev.controller.command;

public enum SortFilter {
	searchString, sortByPrice, sortByCategory, sortByTitle, sortByLogin
}
