package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.tools.Generator;

public class GenerateCommand extends Command {

	public GenerateCommand() {
		permission = null;
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws UserNotFoundException, ItemNotFoundException,
			ProductNotFoundException {
		Generator.generateAccess();
		Generator.generateCategories();
		Generator.generateSpecifications();
		Generator.generateStorage();
		Generator.generateLogistics();
		Generator.generateProducts();
		Generator.generateUsers();
	}
}
