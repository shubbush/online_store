package ru.lanit.shubanev.controller.command;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import ru.lanit.shubanev.controller.DeliveryController;
import ru.lanit.shubanev.controller.UserController;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.view.Invoice;
import ru.lanit.shubanev.view.View;

public class CheckOutCommand extends Command {

	private String resDevTime = "";

	public CheckOutCommand() {
		permission = "checkout";
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws OnlineStoreException {
		long userId = (long) map.get(ExecuteArgument.userId);
		DeliveryController deliveryController = new DeliveryController();
		UserController userController = new UserController();
		deliveryController.checkoutCart(userController.getCart(userId), userId);
		Calendar cal = deliveryController.calculateDeliveryTime(userController
				.getCart(userId).getCart());
		resDevTime = (new SimpleDateFormat("dd.MM.yyyy").format(cal.getTime()));
		map.put(ExecuteArgument.result, Invoice.generateInvoice(resDevTime,
				View.STORE_NAME,
				userController.getCart(View.getCurrentUser().getId())));
		userController.getCart(userId).getCart().clear();
	}

}
