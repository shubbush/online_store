package ru.lanit.shubanev.controller.command;

public enum ExecuteArgument {
	login, password, rpn, result, title, description, price, code, value, productId, specId, storageId, quantity, userId
}
