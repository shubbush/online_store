package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.exceptions.OnlineStoreException;

public class AddSpecCommand extends Command {

	public AddSpecCommand() {
		permission = "addProduct";
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws OnlineStoreException {
		ProductController productController = new ProductController();

		String value = (String) map.get(ExecuteArgument.value);
		long productId = (long) map.get(ExecuteArgument.productId);
		long specId = (long) map.get(ExecuteArgument.specId);
		productController.addProductSpec(productId, specId, value);
	}

}
