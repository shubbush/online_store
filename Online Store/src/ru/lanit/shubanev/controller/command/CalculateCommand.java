package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.calculator.RPN;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.exceptions.WrongInputFormatException;
import ru.lanit.shubanev.exceptions.WrongNumberOfBracketsException;

public class CalculateCommand extends Command {

	private RPN parser = new RPN();
	private String request;

	public CalculateCommand() {
		permission = null;
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws UserNotFoundException, ItemNotFoundException,
			ProductNotFoundException, WrongNumberOfBracketsException,
			WrongInputFormatException {
		map.put(ExecuteArgument.result,
				parser.completeRPN((String) map.get(ExecuteArgument.rpn)));
	}
}
