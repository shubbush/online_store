package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.FrontController;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.PermissionAccessDeniedException;
import ru.lanit.shubanev.model.user.User;

public abstract class Command {

	protected String permission;

	public String getPermission() {
		return permission;
	}

	public void tryExecute(User user, Map<ExecuteArgument, Object> map)
			throws OnlineStoreException {
		FrontController controller = new FrontController();
		if (permission != null) {
			if (!controller.getAccessController().haveUserPermission(
					permission, user)) {
				throw new PermissionAccessDeniedException();
			}
		}
		execute(map);
	}

	public abstract void execute(Map<ExecuteArgument, Object> map)
			throws OnlineStoreException;
}
