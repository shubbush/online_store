package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;

public class AddProductCommand extends Command {

	public AddProductCommand() {
		permission = "createProduct";
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws ProductNotFoundException {
		String title = (String) map.get(ExecuteArgument.title);
		double price = (double) map.get(ExecuteArgument.price);
		String desc = (String) map.get(ExecuteArgument.description);
		String code = (String) map.get(ExecuteArgument.code);
		ProductController productController = new ProductController();
		productController.addProduct(title, price, desc, code);
		map.put(ExecuteArgument.result, productController
				.getProductIdByTitle((String) map.get(ExecuteArgument.title)));
	}
}
