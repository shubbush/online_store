package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.StorageController;
import ru.lanit.shubanev.exceptions.OnlineStoreException;

public class AddLogisticCommand extends Command {

	public AddLogisticCommand() {
		permission = "addProduct";
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws OnlineStoreException {
		StorageController storageController = new StorageController();
		long storageId = (long) map.get(ExecuteArgument.storageId);
		long productId = (long) map.get(ExecuteArgument.productId);
		int quantity = (int) map.get(ExecuteArgument.quantity);
		storageController.addLogistic(productId, storageId, quantity);
	}

}
