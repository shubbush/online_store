package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.controller.StorageController;
import ru.lanit.shubanev.exceptions.OnlineStoreException;

public class DelProductCommand extends Command {

	public DelProductCommand() {
		permission = "delProduct";
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws OnlineStoreException {
		ProductController productController = new ProductController();
		StorageController storageController = new StorageController();
		storageController.deleteProductLogistics((long) map
				.get(ExecuteArgument.productId));
		productController.deleteProduct((long) map
				.get(ExecuteArgument.productId));
	}
}
