package ru.lanit.shubanev.controller.command;

import java.util.Map;

import ru.lanit.shubanev.controller.AccessController;
import ru.lanit.shubanev.controller.UserController;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.user.Cart;
import ru.lanit.shubanev.model.user.User;
import ru.lanit.shubanev.view.View;

public class SignUpCommand extends Command {

	private String role = "customer";

	public SignUpCommand() {
		permission = null;
	}

	@Override
	public void execute(Map<ExecuteArgument, Object> map)
			throws UserNotFoundException, ItemNotFoundException {
		UserController userController = new UserController();
		AccessController accessController = new AccessController();
		userController.addUser((String) map.get(ExecuteArgument.login),
				(String) map.get(ExecuteArgument.password));
		accessController.createUserRole(
				(String) map.get(ExecuteArgument.login), role);
		User user = userController.getUserByLogin((String) map
				.get(ExecuteArgument.login));
		Cart cart = new Cart(user.getId());
		userController.addCart(cart);
		View.setCurrentUser(user);
	}
}
