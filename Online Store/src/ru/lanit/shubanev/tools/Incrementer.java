package ru.lanit.shubanev.tools;

/**
 * Счетчик ID категорий.
 *
 * @author shubbush
 */

public class Incrementer {

	/** The i. */
	private int i;

	/**
	 * Instantiates a new incrementer.
	 */
	public Incrementer() {
		i = 0;
	}

	/**
	 * Gets the next id.
	 *
	 * @return Возвращает следующие значение ID
	 */
	public int getNextId() {
		return i++;
	}

}
