package ru.lanit.shubanev.tools;

import java.util.Comparator;

import ru.lanit.shubanev.model.product.Product;

public class ProductPriceComparator implements Comparator<Product> {

	@Override
	public int compare(Product o1, Product o2) {
		return (int) (o1.getPrice() - o2.getPrice());
	}

}
