package ru.lanit.shubanev.tools;

import java.util.Comparator;

import ru.lanit.shubanev.controller.StorageController;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;

/**
 * The Class LogisticComparator.
 */
public class LogisticComparator implements Comparator<Logistic> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Logistic o1, Logistic o2) {
		StorageController storageController = new StorageController();
		try {
			return storageController.getDeliveryTimeByStorageId(o1
					.getStorageId())
					- storageController.getDeliveryTimeByStorageId(o2
							.getStorageId());
		} catch (StorageNotFoundException e) {
			System.out.println("storage not found");
		}
		return 0;
	}
}
