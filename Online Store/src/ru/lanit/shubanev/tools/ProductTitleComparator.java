package ru.lanit.shubanev.tools;

import java.util.Comparator;

import ru.lanit.shubanev.model.product.Product;

public class ProductTitleComparator implements Comparator<Product> {

	@Override
	public int compare(Product o1, Product o2) {
		return o1.getTitle().compareTo(o2.getTitle());
	}
}
