package ru.lanit.shubanev.tools;

import java.util.Comparator;

import ru.lanit.shubanev.model.user.User;

public class UserLoginComparator implements Comparator<User> {

	@Override
	public int compare(User o1, User o2) {
		return o1.getLogin().compareTo(o2.getLogin());
	}
}
