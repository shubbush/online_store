package ru.lanit.shubanev.tools;

import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.controller.AccessController;
import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.controller.StorageController;
import ru.lanit.shubanev.controller.UserController;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.user.Cart;

/**
 * Генерирует книги, категории и пользователей.
 *
 * @author shubbush
 */

public class Generator {

	/**
	 * Generate categories.
	 *
	 * @return the list
	 */
	public static void generateCategories() {
		ProductController productController = new ProductController();
		productController.addCategory("book", 0);
	}

	/**
	 * Generate specifications.
	 *
	 * @param cat
	 *            the cat
	 * @return the list
	 */
	public static void generateSpecifications() {
		ProductController productController = new ProductController();
		long id = productController.getCategoryIdByTitle("book");
		productController.addSpecification(id, "Author");
		productController.addSpecification(id, "Genre");
		productController.addSpecification(id, "Pages");
		productController.addSpecification(id, "Publisher");
	}

	/**
	 * Generate products.
	 *
	 * @param specs
	 *            the specs
	 * @return the list
	 * @throws ProductNotFoundException
	 */
	public static void generateProducts() throws ProductNotFoundException {
		ProductController productController = new ProductController();
		List<String> authors = new ArrayList<>();
		List<String> genres = new ArrayList<>();
		List<String> pages = new ArrayList<>();
		List<String> publishers = new ArrayList<>();
		List<Long> productsId = new ArrayList<>();
		List<Long> specys = new ArrayList<>();
		specys.add(productController.getSpecIdByTitle("Author"));
		specys.add(productController.getSpecIdByTitle("Genre"));
		specys.add(productController.getSpecIdByTitle("Pages"));
		specys.add(productController.getSpecIdByTitle("Publisher"));
		authors.add("Генри Миллер");
		authors.add("Чарльз Буковски");
		authors.add("Эдуард Лимонов");
		genres.add("Роман");
		genres.add("Детектив");
		genres.add("Роман");
		pages.add("348");
		pages.add("263");
		pages.add("215");
		publishers.add("АСТ");
		publishers.add("АСТ");
		publishers.add("Глагол");
		productController.addProduct("Тропик рака", 200.0, "Автобиография",
				"14sf2sf");
		productController.addProduct("Это я - Эдичка", 80.0,
				"Первый роман Эд. Лимонова", "134ndf2i3n");
		productController.addProduct("макулатура", 175.0,
				"Последний законченный роман", "1fnsdjr23");
		productsId.add(productController.getProductIdByTitle("Тропик рака"));
		productsId.add(productController.getProductIdByTitle("Это я - Эдичка"));
		productsId.add(productController.getProductIdByTitle("макулатура"));
		int i = 0;
		int j = 0;
		for (Long productId : productsId) {
			j = 0;
			productController.addProductSpec(productId, specys.get(j++),
					authors.get(i));
			productController.addProductSpec(productId, specys.get(j++),
					genres.get(i));
			productController.addProductSpec(productId, specys.get(j++),
					pages.get(i));
			productController.addProductSpec(productId, specys.get(j++),
					publishers.get(i));
			i++;
		}
	}

	// storage.addBook("Вопль", "Алан Гинзберг", 350, "Азбука", 100,
	// "Поэзия", "jbkjb348sdf");
	// storage.addBook("Полые люди", "Томас Элиот", 862, "АСТ", 86,
	// "Поэзия", "asd1412fa");
	// storage.addBook("Дама с коготками", "Дарья Донцова", 190, "АСТ", 251,
	// "Детектив", "asflj1fa");

	/**
	 * Generate storage.
	 *
	 * @param prod
	 *            the prod
	 * @return the list
	 */
	public static void generateStorage() {
		StorageController storageController = new StorageController();
		storageController.addStorage("Storage 1", 1);
		storageController.addStorage("Store", 0);
	}

	/**
	 * Generate logistics.
	 *
	 * @param storages
	 *            the storages
	 * @param prod
	 *            the prod
	 * @return the list
	 * @throws ProductNotFoundException
	 */
	public static void generateLogistics() throws ProductNotFoundException {
		StorageController storageController = new StorageController();
		ProductController productController = new ProductController();
		List<Long> productsId = new ArrayList<>();
		List<Long> storagesId = new ArrayList<>();
		productsId.add(productController.getProductIdByTitle("Тропик рака"));
		productsId.add(productController.getProductIdByTitle("Это я - Эдичка"));
		productsId.add(productController.getProductIdByTitle("макулатура"));
		storagesId.add(storageController.getStorageIdByTitle("Store"));
		storagesId.add(storageController.getStorageIdByTitle("Storage 1"));
		storageController.addLogistic(productsId.get(0), storagesId.get(0), 2);
		storageController.addLogistic(productsId.get(0), storagesId.get(1), 2);
		storageController.addLogistic(productsId.get(1), storagesId.get(1), 1);
		storageController.addLogistic(productsId.get(2), storagesId.get(1), 3);
	}

	/**
	 * Generate collection of users.
	 *
	 * @return the list
	 */
	public static void generateUsers() {
		UserController userController = new UserController();
		AccessController accessController = new AccessController();
		userController.addUser("thom", "111");
		long id = userController.getUserByLogin("thom").getId();
		userController.addCart(new Cart(id));
		accessController.createUserRole("thom", "customer");
		userController.addUser("admin", "admin");
		id = userController.getUserByLogin("admin").getId();
		userController.addCart(new Cart(id));
		accessController.createUserRole("admin", "inspector");
	}

	public static void generateAccess() {
		UserController userController = new UserController();
		AccessController accessController = new AccessController();
		String ROOT_LOGIN = "root";
		String ROOT_PWD = "admin";
		userController.addUser(ROOT_LOGIN, ROOT_PWD);
		accessController.createPermission("createProduct");
		accessController.createPermission("viewUsers");
		accessController.createPermission("delProduct");
		accessController.createPermission("editProduct");
		accessController.createPermission("addStorage");
		accessController.createPermission("delStorage");
		accessController.createPermission("editStorage");
		accessController.createPermission("delOrder");
		accessController.createPermission("editUser");
		accessController.createPermission("checkout");
		accessController.createPermission("searchUsers");
		accessController.createPermission("addToCart");
		accessController.createRole("root");
		accessController.createRole("customer");
		accessController.createRole("packer");
		accessController.createRole("inspector");
		accessController.createUserRole("root", "root");
		accessController.createRolePermission("root", "createProduct");
		accessController.createRolePermission("root", "searchUsers");
		accessController.createRolePermission("root", "delProduct");
		accessController.createRolePermission("root", "editProduct");
		accessController.createRolePermission("root", "editUser");
		accessController.createRolePermission("root", "addStorage");
		accessController.createRolePermission("root", "delStorage");
		accessController.createRolePermission("root", "editStorage");
		accessController.createRolePermission("root", "delOrder");
		accessController.createRolePermission("root", "viewUsers");
		accessController.createRolePermission("customer", "checkout");
		accessController.createRolePermission("customer", "addToCart");
		accessController.createRolePermission("packer", "delProduct");
		accessController.createRolePermission("packer", "editProduct");
		accessController.createRolePermission("inspector", "createProduct");
	}

}
