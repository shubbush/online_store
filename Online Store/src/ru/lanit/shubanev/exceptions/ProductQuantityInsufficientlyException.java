package ru.lanit.shubanev.exceptions;

/**
 * The Class ProductQuantityInsufficientlyException.
 */
public class ProductQuantityInsufficientlyException extends
		OnlineStoreException {

	/**
	 * Instantiates a new product quantity insufficiently exception.
	 */
	public ProductQuantityInsufficientlyException() {
	}

	/**
	 * Instantiates a new product quantity insufficiently exception.
	 *
	 * @param message
	 *            the message
	 */
	public ProductQuantityInsufficientlyException(String message) {
		super(message);
	}

}
