package ru.lanit.shubanev.exceptions;

/**
 * The Class WrongInputFormatException.
 */
public class WrongInputFormatException extends OnlineStoreException {

	/**
	 * Instantiates a new wrong input format exception.
	 */
	public WrongInputFormatException() {
	}

	/**
	 * Instantiates a new wrong input format exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public WrongInputFormatException(String msg) {
		super(msg);
	}

}
