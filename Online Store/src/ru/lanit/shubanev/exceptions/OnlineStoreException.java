package ru.lanit.shubanev.exceptions;

public class OnlineStoreException extends Exception {

	public OnlineStoreException() {

	}

	public OnlineStoreException(String msg) {
		super(msg);
	}

}
