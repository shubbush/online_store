package ru.lanit.shubanev.exceptions;

/**
 * The Class ItemNotFoundException.
 */
public class ItemNotFoundException extends OnlineStoreException {

	/**
	 * Instantiates a new product not found exception.
	 */
	public ItemNotFoundException() {
	}

	/**
	 * Instantiates a new product not found exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public ItemNotFoundException(String msg) {
		super(msg);
	}

}
