package ru.lanit.shubanev.exceptions;

/**
 * The Class WrongNumberOfBracketsException.
 */
public class WrongNumberOfBracketsException extends OnlineStoreException {

	/**
	 * Instantiates a new wrong number of brackets exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public WrongNumberOfBracketsException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new wrong number of brackets exception.
	 */
	public WrongNumberOfBracketsException() {
	}
}
