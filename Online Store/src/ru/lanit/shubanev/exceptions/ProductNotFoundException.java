package ru.lanit.shubanev.exceptions;

/**
 * The Class ProductNotFoundException.
 */
public class ProductNotFoundException extends OnlineStoreException {

	/**
	 * Instantiates a new product id not found exception.
	 */
	public ProductNotFoundException() {
	}

	/**
	 * Instantiates a new product id not found exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public ProductNotFoundException(String msg) {
		super(msg);
	}
}
