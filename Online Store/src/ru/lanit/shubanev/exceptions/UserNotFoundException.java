package ru.lanit.shubanev.exceptions;

/**
 * The Class UserNotFoundException.
 */
public class UserNotFoundException extends OnlineStoreException {

	/**
	 * Instantiates a new user not found exception.
	 */
	public UserNotFoundException() {
	}

	/**
	 * Instantiates a new user not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public UserNotFoundException(String message) {
		super(message);
	}
}
