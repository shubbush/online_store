package ru.lanit.shubanev.view;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.user.Cart;

/**
 * The Class Invoice.
 */
public class Invoice {

	private final static int FULL_STRING_LENGTH = 40;
	private final static int CENTER_STRING_LENGTH = 39;
	private final static int PRODUCT_STRING_LENGTH = 38;
	private final static int PRODUCT_WITH_COUNT_STRING_LENGTH = 36;

	/**
	 * Generate invoice.
	 *
	 * @param devTime
	 *            the dev time
	 * @param storeName
	 *            the store name
	 * @param cart
	 *            the cart
	 * @return the string
	 */
	public static String generateInvoice(String devTime, String storeName,
			Cart cart) {
		ProductController productController = new ProductController();
		StringBuilder res = new StringBuilder();
		Double total = 0.0;
		appendHyphen(res);
		appendCenter("Welcome!", res);
		appendCenter(storeName, res);
		for (Logistic prodinfo : cart.getCart()) {
			Product product;
			try {
				product = productController.getProductById(prodinfo
						.getProductId());
				total += product.getPrice() * prodinfo.getQuantity();
				appendStr(
						product.getTitle(),
						Integer.toString(prodinfo.getQuantity()),
						Double.toString(product.getPrice()
								* prodinfo.getQuantity()), res);
			} catch (ProductNotFoundException e) {
				System.out.println("Product not exist");
			}
		}

		appendHyphen(res);
		appendStr("Total", Double.toString(total), res);
		appendHyphen(res);
		appendStr("DeliveryController time", devTime, res);
		appendCenter("Thank you!", res);
		appendHyphen(res);
		return res.toString();
	}

	/**
	 * Append hyphen.
	 *
	 * @param res
	 *            the res
	 */
	private static void appendHyphen(StringBuilder res) {
		for (int i = 0; i < FULL_STRING_LENGTH; i++) {
			res.append("-");
		}
		res.append("\n");
	}

	/**
	 * Append str.
	 *
	 * @param str1
	 *            the str1
	 * @param str2
	 *            the str2
	 * @param str3
	 *            the str3
	 * @param res
	 *            the res
	 */
	private static void appendStr(String str1, String str2, String str3,
			StringBuilder res) {
		res.append("|");
		res.append(str1);
		res.append(".x");
		res.append(str2);
		for (int i = 0; i < PRODUCT_WITH_COUNT_STRING_LENGTH - str1.length()
				- str2.length() - str3.length(); i++) {
			res.append(".");
		}
		res.append(str3);
		res.append("|" + "\n");
	}

	/**
	 * Append str.
	 *
	 * @param str1
	 *            the str1
	 * @param str2
	 *            the str2
	 * @param res
	 *            the res
	 */
	private static void appendStr(String str1, String str2, StringBuilder res) {
		res.append("|");
		res.append(str1);
		for (int i = 0; i < PRODUCT_STRING_LENGTH - str1.length()
				- str2.length(); i++) {
			res.append(".");
		}
		res.append(str2);
		res.append("|" + "\n");
	}

	/**
	 * Append center.
	 *
	 * @param str
	 *            the str
	 * @param res
	 *            the res
	 */
	private static void appendCenter(String str, StringBuilder res) {
		res.append("|");
		for (int i = 0; i < CENTER_STRING_LENGTH - str.length(); i++) {
			if (i == 15) {
				res.append(str);
			} else {
				res.append(" ");
			}
		}
		res.append("|" + "\n");
	}
}
