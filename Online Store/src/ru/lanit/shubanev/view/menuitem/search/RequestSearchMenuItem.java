package ru.lanit.shubanev.view.menuitem.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.view.PagedView;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class RequestSearchMenuItem extends MenuItem {

	public RequestSearchMenuItem() {
		menuItemTitle = "Search by request";
		command = null;
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		Map<SortFilter, Object> map = new HashMap<>();
		System.out.println("Search: ");
		try {
			String request = scan.readLine();
			map.put(SortFilter.searchString, request);
			new PagedView<Product>(Product.class, map).showPage();

		} catch (IOException e) {
			System.out.println("Input error, pls try again");
		}
	}
}
