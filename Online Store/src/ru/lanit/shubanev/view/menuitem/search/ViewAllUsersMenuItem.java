package ru.lanit.shubanev.view.menuitem.search;

import java.io.IOException;
import java.util.HashMap;

import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.model.user.User;
import ru.lanit.shubanev.view.PagedView;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class ViewAllUsersMenuItem extends MenuItem {

	public ViewAllUsersMenuItem() {
		menuItemTitle = "View all users";
		command = null;
	}

	@Override
	public void chooseMenuItem() {
		try {
			new PagedView<User>(User.class, new HashMap<SortFilter, Object>())
					.showPage();
		} catch (IOException e) {
			System.out.println("Error, pls try again");
		}
	}
}
