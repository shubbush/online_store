package ru.lanit.shubanev.view.menuitem.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.DelProductCommand;
import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.view.PagedView;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class DelProductMenuItem extends MenuItem {

	public DelProductMenuItem() {
		menuItemTitle = "Delete product";
		command = new DelProductCommand();
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		System.out.println("Choose product number: ");
		Map<ExecuteArgument, Object> map = new HashMap<>();
		PagedView<Product> page = new PagedView<>(Product.class,
				new HashMap<SortFilter, Object>());
		try {
			page.showPage();
			long productId = Long.parseLong(scan.readLine());
			map.put(ExecuteArgument.productId, productId);
			command.tryExecute(View.getCurrentUser(), map);
		} catch (IOException e) {
			System.out.println("Input error, pls try again");
		} catch (OnlineStoreException e) {
			System.out.println("Error try again");
		}
	}

}
