package ru.lanit.shubanev.view.menuitem.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.view.PagedView;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class SearchByCategoryMenuItem extends MenuItem {

	public SearchByCategoryMenuItem() {
		menuItemTitle = "Search by category";
		command = null;
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		Map<SortFilter, Object> map = new HashMap<>();
		System.out.println("Choose category: ");
		System.out.println(getCategories());
		try {
			int category = Integer.parseInt(scan.readLine());
			map.put(SortFilter.sortByCategory, category);
			new PagedView<Product>(Product.class, map).showPage();
		} catch (NumberFormatException | IOException e) {
			System.out.println("Input error, pls try again");
		}

	}

	private String getCategories() {
		ProductController productController = new ProductController();
		String categories = productController.getCategoriesString();
		return categories;
	}
}
