package ru.lanit.shubanev.view.menuitem.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.view.menuitem.MenuItem;

public abstract class SearchMenuItem extends MenuItem {

	protected List<MenuItem> listMenu = new ArrayList<>();

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		System.out.println("Select type of searching: ");
		int i = 1;
		for (MenuItem menuItem : listMenu) {
			System.out.println(i + ". " + menuItem.getMenuItemTitle());
			i++;
		}
		int choose;
		try {
			choose = Integer.parseInt(scan.readLine());
			listMenu.get(choose - 1).chooseMenuItem();
		} catch (NumberFormatException | IOException e) {
			System.out.println("Input error, try again");
		}
	}
}
