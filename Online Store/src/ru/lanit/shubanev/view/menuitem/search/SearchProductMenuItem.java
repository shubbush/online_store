package ru.lanit.shubanev.view.menuitem.search;


public class SearchProductMenuItem extends SearchMenuItem {

	public SearchProductMenuItem() {
		menuItemTitle = "Search product";
		command = null;
		listMenu.add(new RequestSearchMenuItem());
		listMenu.add(new SearchByCategoryMenuItem());
	}

}
