package ru.lanit.shubanev.view.menuitem.search;

public class SearchUserMenuItem extends SearchMenuItem {

	public SearchUserMenuItem() {
		menuItemTitle = "Search user";
		command = null;
		listMenu.add(new ViewAllUsersMenuItem());
	}
}
