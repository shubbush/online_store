package ru.lanit.shubanev.view.menuitem;

import java.util.HashMap;

import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.controller.command.GenerateCommand;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.PermissionAccessDeniedException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.view.View;

public class GenerateMenuItem extends MenuItem {

	public GenerateMenuItem() {
		menuItemTitle = "Generate";
		command = new GenerateCommand();
	}

	@Override
	public void chooseMenuItem() {
		try {
			command.tryExecute(View.getCurrentUser(),
					new HashMap<ExecuteArgument, Object>());
		} catch (PermissionAccessDeniedException | UserNotFoundException
				| ItemNotFoundException e) {
			System.out.println("Access denied");
		} catch (OnlineStoreException e) {
			System.out.println("Error^ try again");
		}
	}
}
