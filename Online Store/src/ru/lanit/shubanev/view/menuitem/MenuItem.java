package ru.lanit.shubanev.view.menuitem;

import ru.lanit.shubanev.controller.AccessController;
import ru.lanit.shubanev.controller.command.Command;
import ru.lanit.shubanev.view.View;

public abstract class MenuItem {

	protected String menuItemTitle;
	protected Command command;

	public String getMenuItemTitle() {
		return menuItemTitle;
	}

	public boolean havePermission() {
		AccessController accessController = new AccessController();
		if (command == null) {
			return true;
		}
		return accessController.haveUserPermission(command.getPermission(),
				View.getCurrentUser());
	}

	public abstract void chooseMenuItem();

}
