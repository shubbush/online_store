package ru.lanit.shubanev.view.menuitem;

public class ExitMenuItem extends MenuItem {

	public ExitMenuItem() {
		menuItemTitle = "Exit";
		command = null;
	}

	@Override
	public void chooseMenuItem() {
		System.exit(0);
	}
}
