package ru.lanit.shubanev.view.menuitem.profileview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.ProductController;
import ru.lanit.shubanev.controller.StorageController;
import ru.lanit.shubanev.controller.command.AddLogisticCommand;
import ru.lanit.shubanev.controller.command.AddProductCommand;
import ru.lanit.shubanev.controller.command.AddSpecCommand;
import ru.lanit.shubanev.controller.command.Command;
import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.PermissionAccessDeniedException;
import ru.lanit.shubanev.model.logistic.Storage;
import ru.lanit.shubanev.model.product.Spec;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class AddProductMenuItem extends MenuItem {

	public AddProductMenuItem() {
		menuItemTitle = "Add product to store";
		command = new AddProductCommand();
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(
				System.in));
		ProductController productController = new ProductController();
		StorageController storageController = new StorageController();
		Command addSpec = new AddSpecCommand();
		Command addLogistic = new AddLogisticCommand();
		try {
			Map<ExecuteArgument, Object> mapProduct = new HashMap<>();
			Map<ExecuteArgument, Object> mapSpecs = new HashMap<>();
			Map<ExecuteArgument, Object> mapStorage = new HashMap<>();
			System.out.println("Enter title: ");
			mapProduct.put(ExecuteArgument.title, scanner.readLine());
			System.out.println("Enter description: ");
			mapProduct.put(ExecuteArgument.description, scanner.readLine());
			System.out.println("Enter price: ");
			mapProduct.put(ExecuteArgument.price,
					Double.parseDouble(scanner.readLine()));
			System.out.println("Enter code: ");
			String code = scanner.readLine();
			System.out.println("Choose category: ");
			command.tryExecute(View.getCurrentUser(), mapProduct);
			long productId = (long) mapProduct.get(ExecuteArgument.result);
			System.out.println(productController.getCategoriesString());
			long categoryId = Long.parseLong(scanner.readLine());
			for (Spec spec : productController.getSpecsByCategoryId(categoryId)) {
				mapSpecs.clear();
				System.out.println("Enter " + spec.getTitle());
				mapSpecs.put(ExecuteArgument.value, scanner.readLine());
				mapSpecs.put(ExecuteArgument.productId, productId);
				mapSpecs.put(ExecuteArgument.specId, spec.getCode());
				addSpec.execute(mapSpecs);
			}
			System.out.println("Choose storage id: ");
			System.out.println(storagesToString());
			mapStorage.put(ExecuteArgument.storageId,
					Long.parseLong(scanner.readLine()));
			System.out.println("Enter quantity: ");
			mapStorage.put(ExecuteArgument.quantity,
					Integer.parseInt(scanner.readLine()));
			mapStorage.put(ExecuteArgument.productId, productId);
			addLogistic.execute(mapStorage);
		} catch (PermissionAccessDeniedException e) {
			System.out.println("You don't have rights for this action");
		} catch (IOException e) {
			System.out.println("Input error, pls try again");
		} catch (OnlineStoreException e) {
			System.out.println("Error, pls try again");
		}
	}

	private String storagesToString() {
		StorageController controller = new StorageController();
		StringBuilder result = new StringBuilder();
		for (Storage storage : controller.getStorages()) {
			result.append(storage.getId() + ". " + storage.getTitle() + "\n");
		}
		return result.toString();
	}

}
