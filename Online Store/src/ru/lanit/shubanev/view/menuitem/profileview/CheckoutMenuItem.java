package ru.lanit.shubanev.view.menuitem.profileview;

import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.CheckOutCommand;
import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class CheckoutMenuItem extends MenuItem {

	public CheckoutMenuItem() {
		menuItemTitle = "Proceed to checkout";
		command = new CheckOutCommand();
	}

	@Override
	public void chooseMenuItem() {
		Map<ExecuteArgument, Object> map = new HashMap<>();
		try {
			map.put(ExecuteArgument.userId, View.getCurrentUser().getId());
			command.tryExecute(View.getCurrentUser(), map);
			System.out.println(map.get(ExecuteArgument.result));
		} catch (ProductQuantityInsufficientlyException e) {
			System.out
					.println("We don't have "
							+ e.getMessage()
							+ " in the right quantity. Our mannager call you later and correct your order ");
		} catch (OnlineStoreException e) {
			e.printStackTrace();
		}
	}
}
