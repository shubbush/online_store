package ru.lanit.shubanev.view.menuitem.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.controller.command.SignInCommand;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.user.User;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class SignInMenuItem extends MenuItem {

	public SignInMenuItem() {
		menuItemTitle = "Sign in";
		command = new SignInCommand();

	}

	@Override
	public void chooseMenuItem() {
		if (View.getCurrentUser() == null) {
			BufferedReader scan = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				Map<ExecuteArgument, Object> map = new HashMap<>();
				System.out.println("Input login: ");
				map.put(ExecuteArgument.login, scan.readLine());
				System.out.println("Input password: ");
				map.put(ExecuteArgument.password, scan.readLine());
				command.tryExecute(View.getCurrentUser(), map);
				View.setCurrentUser((User) map.get(ExecuteArgument.result));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (UserNotFoundException e) {
				System.out.println("User with login \"" + e.getMessage()
						+ "\" not found or password is wrong");
			} catch (OnlineStoreException e) {
				e.printStackTrace();
			}

		} else {
			System.out.println("You are already signIn");
		}
	}
}
