package ru.lanit.shubanev.view.menuitem.user;

import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class LogOutMenuItem extends MenuItem {

	public LogOutMenuItem() {
		menuItemTitle = "Logout";
		command = null;
	}

	@Override
	public void chooseMenuItem() {
		View.setCurrentUser(null);
	}

}
