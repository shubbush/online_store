package ru.lanit.shubanev.view.menuitem.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.controller.command.SignUpCommand;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class SignUpMenuItem extends MenuItem {

	public SignUpMenuItem() {
		menuItemTitle = "Sign Up";
		command = new SignUpCommand();
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		try {
			Map<ExecuteArgument, Object> map = new HashMap<>();
			System.out.println("Input login: ");
			map.put(ExecuteArgument.login, scan.readLine());
			System.out.println("Input password: ");
			map.put(ExecuteArgument.password, scan.readLine());
			command.tryExecute(View.getCurrentUser(), map);
		} catch (IOException e) {
			System.out.println("Input error, pls try again");
			e.printStackTrace();
		} catch (OnlineStoreException e) {
			System.out.println("Error, pls try again");
		}

	}
}
