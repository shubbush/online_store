package ru.lanit.shubanev.view.menuitem.user;

import ru.lanit.shubanev.view.ProfileView;
import ru.lanit.shubanev.view.View;
import ru.lanit.shubanev.view.menuitem.MenuItem;

public class ProfileViewMenuItem extends MenuItem {

	public ProfileViewMenuItem() {
		menuItemTitle = "View profile";
	}

	@Override
	public void chooseMenuItem() {
		if (View.getCurrentUser() != null) {
			ProfileView profile = new ProfileView();
			profile.showProfile();

		} else {
			System.out.println("You don't login already");
		}
	}

}
