package ru.lanit.shubanev.view.menuitem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ru.lanit.shubanev.controller.command.CalculateCommand;
import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.exceptions.WrongInputFormatException;
import ru.lanit.shubanev.exceptions.WrongNumberOfBracketsException;
import ru.lanit.shubanev.view.View;

public class CalculatorMenuItem extends MenuItem {

	public CalculatorMenuItem() {
		menuItemTitle = "Calculate";
		command = new CalculateCommand();
	}

	@Override
	public void chooseMenuItem() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		String prs = "";
		Map<ExecuteArgument, Object> map = new HashMap<>();
		try {
			System.out.println("Input string:");
			prs = scan.readLine();
			map.put(ExecuteArgument.rpn, prs);
			command.tryExecute(View.getCurrentUser(), map);
			System.out.println("Result:" + map.get(ExecuteArgument.result));
		} catch (ProductNotFoundException e) {
			System.out.println("Book with id: \"" + e.getMessage()
					+ "\" doesnt exist");
		} catch (WrongNumberOfBracketsException e) {
			System.out.println("Wrong input. Check number of brackets");
		} catch (WrongInputFormatException e) {
			System.out.println("Wrong input. Check request");
		} catch (IOException e) {
			System.out.println("Input error. Pls try again");
		} catch (OnlineStoreException e) {
			System.out.println("error");
		}
	}
}
