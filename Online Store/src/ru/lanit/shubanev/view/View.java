package ru.lanit.shubanev.view;

import ru.lanit.shubanev.controller.AccessController;
import ru.lanit.shubanev.model.user.User;

/**
 * Отображение работы программы в консоль.
 *
 * @author shubbush
 */
public class View {

	/** The current user. */
	protected static User currentUser = null;

	/** The store name. */
	public final static String STORE_NAME = "Black Books";

	/**
	 * Запуск программы, вывод меню, обработка команд.
	 */
	public void startApp() {
		AccessController controller = new AccessController();
		System.out.println(STORE_NAME);
		new MainMenuView().showMainMenu();
	}

	public static User getCurrentUser() {
		return currentUser;
	}

	public static void setCurrentUser(User user) {
		View.currentUser = user;
	}

}
