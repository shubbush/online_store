package ru.lanit.shubanev.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.view.menuitem.MenuItem;
import ru.lanit.shubanev.view.menuitem.profileview.AddProductMenuItem;
import ru.lanit.shubanev.view.menuitem.profileview.CheckoutMenuItem;
import ru.lanit.shubanev.view.menuitem.search.DelProductMenuItem;
import ru.lanit.shubanev.view.menuitem.search.SearchUserMenuItem;

public class ProfileView {

	private List<MenuItem> listMenu = new ArrayList<>();

	public ProfileView() {
		listMenu.add(new AddProductMenuItem());
		listMenu.add(new CheckoutMenuItem());
		listMenu.add(new SearchUserMenuItem());
		listMenu.add(new DelProductMenuItem());
	}

	public void showProfile() {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(
				System.in));
		List<MenuItem> currentUserProfileMenu = new ArrayList<>();
		for (MenuItem menuItem : listMenu) {
			if (menuItem.havePermission()) {
				currentUserProfileMenu.add(menuItem);
			}
		}
		while (true) {
			int i = 1;
			System.out.println("Choose action:");
			for (MenuItem menuItem : currentUserProfileMenu) {
				System.out.println(i + ". " + menuItem.getMenuItemTitle());
				i++;
			}
			System.out.println("0. Back");
			try {
				int flag = Integer.parseInt(scanner.readLine());
				if (flag == 0) {
					break;
				}
				currentUserProfileMenu.get(flag - 1).chooseMenuItem();
			} catch (NumberFormatException e) {
				System.out.println("Input erros, try again");
			} catch (IOException e) {
				System.out.println("Input erros, try again");
			}
		}
	}
}
