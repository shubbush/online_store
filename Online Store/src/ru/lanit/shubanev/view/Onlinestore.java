package ru.lanit.shubanev.view;

/**
 * The Class Onlinestore.
 */
public class Onlinestore {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		View view = new View();
		view.startApp();
	}
}
