package ru.lanit.shubanev.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import ru.lanit.shubanev.controller.FrontController;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.view.menuitem.CalculatorMenuItem;
import ru.lanit.shubanev.view.menuitem.ExitMenuItem;
import ru.lanit.shubanev.view.menuitem.GenerateMenuItem;
import ru.lanit.shubanev.view.menuitem.MenuItem;
import ru.lanit.shubanev.view.menuitem.search.SearchProductMenuItem;
import ru.lanit.shubanev.view.menuitem.user.LogOutMenuItem;
import ru.lanit.shubanev.view.menuitem.user.ProfileViewMenuItem;
import ru.lanit.shubanev.view.menuitem.user.SignInMenuItem;
import ru.lanit.shubanev.view.menuitem.user.SignUpMenuItem;

public class MainMenuView {

	/** The current user. */

	private FrontController controller = new FrontController();

	private List<MenuItem> listMenu;

	public MainMenuView() {

	}

	private void generateMenu() {
		listMenu = new LinkedList<>();
		listMenu.add(new SearchProductMenuItem());
		listMenu.add(new CalculatorMenuItem());
		listMenu.add(new GenerateMenuItem());
		listMenu.add(new ExitMenuItem());
		if (View.currentUser == null) {
			listMenu.add(0, new SignInMenuItem());
			listMenu.add(1, new SignUpMenuItem());
		} else {
			listMenu.add(listMenu.size() - 1, new ProfileViewMenuItem());
			listMenu.add(listMenu.size() - 1, new LogOutMenuItem());
		}
	}

	public void showMainMenu() {
		BufferedReader scan = new BufferedReader(new InputStreamReader(
				System.in));
		while (true) {
			generateMenu();
			int i = 1;
			System.out.println("Choose action:");
			for (MenuItem menuItem : listMenu) {
				System.out.println(i + ". " + menuItem.getMenuItemTitle());
				i++;
			}
			try {
				int flag = Integer.parseInt(scan.readLine());
				listMenu.get(flag - 1).chooseMenuItem();
			} catch (NumberFormatException e) {
				System.out.println("Input erros, try again");
			} catch (IOException e) {
				System.out.println("Input erros, try again");
			}
		}
	}

	/**
	 * Customer view.
	 *
	 * @param scan
	 *            the scan
	 * @param tmp
	 *            the tmp
	 * @throws IOException
	 */
	private void customerView(BufferedReader scan, List<Product> tmp)
			throws IOException {
		System.out.println("1. Add book");
		System.out.println("0. Cancel");
		switch (scan.read()) {
		case 1: {
			System.out.println("Type book number from list  above");
			int number = scan.read() - 1;
			System.out.println("Input quantity: ");
			int quantity = scan.read();
			if (quantity < 1
					|| controller.getStorageController().getProductCount(
							tmp.get(number).getId()) < quantity) {
				System.out.println("Wrong input");
				break;
			}
			controller.getUserController()
					.addProductToUserCart(tmp.get(number).getId(), quantity,
							View.currentUser.getId());
			break;
		}
		case 0: {
			break;
		}
		}
	}

}
