package ru.lanit.shubanev.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import ru.lanit.shubanev.controller.FrontController;
import ru.lanit.shubanev.controller.PagedController;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.model.ListItem;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.user.User;

public class PagedView<T extends ListItem> {
	private final static int QUANTITY_OF_ELEMENTS_ON_PAGE = 10;
	private FrontController controller = new FrontController();
	private BufferedReader scan = new BufferedReader(new InputStreamReader(
			System.in));
	private Class<T> classType;
	private List<T> listPage;
	private int elementNumber = 0;
	private Map<SortFilter, Object> map;

	public PagedView(Class<T> classType, Map<SortFilter, Object> map) {
		this.classType = classType;
		this.map = map;
	}

	public void showPage() throws IOException {
		PagedController<T> pageController = new PagedController<>(classType);
		try {
			int count = pageController.getItemsCountForRequest(map);
			boolean flag = true;
			while (flag) {

				listPage = pageController.getPage(elementNumber,
						QUANTITY_OF_ELEMENTS_ON_PAGE, map);
				printItems(listPage, elementNumber, classType);
				if (elementNumber + QUANTITY_OF_ELEMENTS_ON_PAGE + 1 < count
						&& count > QUANTITY_OF_ELEMENTS_ON_PAGE)
					System.out.println("1. Next page");
				if (elementNumber > 0) {
					System.out.println("2.Previous page");
				}
				// System.out.println(controller.getAccessController()
				// .getUserMenu(View.currentUser));
				System.out.println("0. Back");
				int number = Integer.parseInt(scan.readLine());
				switch (number) {
				case 1: {
					if (elementNumber + QUANTITY_OF_ELEMENTS_ON_PAGE + 1 < count)
						elementNumber += 10;
					break;
				}
				case 2: {
					if (elementNumber > 0)
						elementNumber -= 10;
					break;
				}
				case 0: {
					flag = false;
				}
				}
			}
		} catch (ItemNotFoundException e) {
			System.out.println("Not found");
		}

	}

	public T getItemId(int numberAtList) {
		return listPage.get(numberAtList - elementNumber);
	}

	private void printItems(List<T> listPage2, int number, Class<T> clazz) {
		for (T element : listPage2) {
			if (element != null) {
				if (clazz.getName().equals(Product.class.getName())) {
					int count = controller.getStorageController()
							.getProductCount(((Product) element).getId());
					System.out.println(number + ". " + element.toString()
							+ " Count: " + count);
				} else {
					StringBuilder str = new StringBuilder();
					str.append(number + ". " + "Login: "
							+ ((User) element).getLogin() + " Roles: ");
					for (String role : controller.getAccessController()
							.getStringUserRoles((User) element)) {
						str.append(role + " ");
					}

					System.out.println(str.toString());
				}
				number++;
			}
		}
	}
}
