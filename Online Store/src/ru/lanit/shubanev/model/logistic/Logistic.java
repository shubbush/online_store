package ru.lanit.shubanev.model.logistic;

/**
 * The Class Logistic.
 */
public class Logistic {

	/** The product. */
	private long productId;

	/** The storageId. */
	private long storageId;

	/** The quantity. */
	private int quantity;

	/**
	 * Instantiates a new logistic.
	 *
	 * @param storageId
	 *            the storageId
	 * @param productId
	 *            the productId
	 * @param quantity
	 *            the quantity
	 */
	public Logistic(long storageId, long productId, int quantity) {
		super();
		this.productId = productId;
		this.storageId = storageId;
		this.quantity = quantity;
	}

	/**
	 * Gets the product.
	 *
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}

	/**
	 * Sets the product.
	 *
	 * @param product
	 *            the new product
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the storageId.
	 *
	 * @return the storageId
	 */
	public long getStorageId() {
		return storageId;
	}

	/**
	 * Sets the storageId.
	 *
	 * @param storageId
	 *            the new storageId
	 */
	public void setStorage(long storageId) {
		this.storageId = storageId;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
