package ru.lanit.shubanev.model.logistic;

/**
 * The Class Storage.
 */
public class Storage {

	/** The delivery time. */
	private int deliveryTime;

	/** The id. */
	private long id;

	/** The title. */
	private String title;

	/**
	 * Instantiates a new storage.
	 *
	 * @param deliveryTime
	 *            the delivery time
	 * @param title
	 *            the title
	 */
	public Storage(int deliveryTime, long id, String title) {
		super();
		this.deliveryTime = deliveryTime;
		this.id = id;
		this.title = title;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the delivery time.
	 *
	 * @return the deliveryTime
	 */
	public int getDeliveryTime() {
		return deliveryTime;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
