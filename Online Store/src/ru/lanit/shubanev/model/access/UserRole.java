package ru.lanit.shubanev.model.access;

/**
 * The Class UserRole.
 */
public class UserRole {

	/** The userId. */
	private long userId;

	/** The roleId. */
	private long roleId;

	/**
	 * Instantiates a new userId roleId.
	 *
	 * @param userId
	 *            the userId
	 * @param roleId
	 *            the roleId
	 */
	public UserRole(long userId, long roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

	/**
	 * Gets the userId.
	 *
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 *
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * Gets the roleId.
	 *
	 * @return the roleId
	 */
	public long getRoleId() {
		return roleId;
	}

	/**
	 * Sets the roleId.
	 *
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

}
