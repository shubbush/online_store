package ru.lanit.shubanev.model.access;

/**
 * The Class Role.
 */
public class Role {

	/** The id. */
	private long id;

	/** The title. */
	private String title;

	/**
	 * Instantiates a new role.
	 *
	 * @param id
	 *            the id
	 * @param title
	 *            the title
	 */
	public Role(long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
