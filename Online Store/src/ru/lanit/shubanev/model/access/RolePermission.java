package ru.lanit.shubanev.model.access;

/**
 * The Class RolePermission.
 */
public class RolePermission {

	/** The roleId. */
	private long roleId;

	/** The command. */
	private long permissionId;

	/**
	 * @param roleId
	 * @param command
	 */
	public RolePermission(long roleId, long permissionId) {
		super();
		this.roleId = roleId;
		this.permissionId = permissionId;
	}

	/**
	 * @return the roleId
	 */
	public long getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRole(long roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the command
	 */
	public long getPermissionId() {
		return permissionId;
	}

	/**
	 * @param command
	 *            the command to set
	 */
	public void setPermission(long permissionId) {
		this.permissionId = permissionId;
	}

}