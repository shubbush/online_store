package ru.lanit.shubanev.model.access;

/**
 * The Class Permission.
 */
public class Permission {

	/** The id. */
	private long id;

	/** The command. */
	private String permission;

	/**
	 * Instantiates a new command.
	 *
	 * @param id
	 *            the id
	 * @param command
	 *            the command
	 */
	public Permission(long id, String permission) {
		super();
		this.id = id;
		this.permission = permission;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the command.
	 *
	 * @return the command
	 */
	public String getPermission() {
		return permission;
	}

	/**
	 * Sets the command.
	 *
	 * @param command
	 *            the command to set
	 */
	public void setPermission(String permission) {
		this.permission = permission;
	}

}