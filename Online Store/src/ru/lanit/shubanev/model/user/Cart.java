package ru.lanit.shubanev.model.user;

import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.model.logistic.Logistic;

/**
 * The Class Cart.
 */
public class Cart {

	/** The userId. */
	private long userId;

	/** The cart. */
	private List<Logistic> cart;

	/**
	 * Instantiates a new cart.
	 *
	 * @param userId
	 *            the userId
	 */
	public Cart(long userId) {
		super();
		this.userId = userId;
		this.cart = new ArrayList<Logistic>();
	}

	public Cart(long userId, List<Logistic> logistics) {
		this.userId = userId;
		this.cart = logistics;
	}

	/**
	 * Gets the userId.
	 *
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 *
	 * @param userId
	 *            the userId to set
	 */
	public void setUser(long userId) {
		this.userId = userId;
	}

	/**
	 * Gets the cart.
	 *
	 * @return the cart
	 */
	public List<Logistic> getCart() {
		return cart;
	}

	/**
	 * Sets the cart.
	 *
	 * @param cart
	 *            the cart to set
	 */
	public void setCart(List<Logistic> cart) {
		this.cart = cart;
	}

	/**
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (cart.size() != 0) {
			for (Logistic prod : cart) {
				builder.append(prod.getProductId() + " Quantity: "
						+ prod.getQuantity());
			}
		}
		return builder.toString();
	}

}
