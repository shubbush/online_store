package ru.lanit.shubanev.model.user;

import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;

import ru.lanit.shubanev.model.ListItem;

/**
 * Родительский класс для всех типов аккаунтов.
 *
 * @author shubbush
 */
@Entity
@Table
public class User implements ListItem {
	/** The id */
	private long id;

	/** The login. */
	protected String login;

	/** The password. */
	protected String password;

	/**
	 * User Constructor
	 * 
	 * @param login
	 *            user login
	 * @param pwd
	 *            user password in md5
	 */
	public User(long id, String login, String pwd) {
		this.id = id;
		this.login = login;
		this.password = pwd;
	}

	/**
	 * Gets the login.
	 *
	 * @return Логин
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login.
	 *
	 * @param login
	 *            Логин
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets the password.
	 *
	 * @return MD5 хэш пароля
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            MD5 хэш пароля
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [login=");
		builder.append(login);
		builder.append("]");
		return builder.toString();
	}
}
