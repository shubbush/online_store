package ru.lanit.shubanev.model.product;

/**
 * Category class.
 *
 * @author shubbush
 */
public class Category {

	/** The id. */
	private long id;

	/** The title. */
	private String title;

	/** The parent. */
	private long parentId;

	/**
	 * Instantiates a new category.
	 *
	 * @param id
	 *            ßthe id
	 * @param title
	 *            the title
	 * @param parent
	 *            the parent
	 */
	public Category(long id, String title, long parentId) {
		super();
		this.id = id;
		this.title = title;
		this.parentId = parentId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public long getParentId() {
		return parentId;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(long parentId) {
		this.parentId = parentId;
	}
}
