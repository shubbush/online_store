package ru.lanit.shubanev.model.product;

/**
 * The Class ProductSpec.
 */
public class ProductSpec {

	private long productId;

	/** The specId. */
	private long specId;

	/** The value. */
	private String value;

	/**
	 * Instantiates a new product specId.
	 *
	 * @param specId
	 *            the specId
	 * @param value
	 *            the value
	 */
	public ProductSpec(long productId, long specId, String value) {
		super();
		this.specId = specId;
		this.value = value;
	}

	/**
	 * Gets the specId.
	 *
	 * @return the specId
	 */
	public long getSpecId() {
		return specId;
	}

	/**
	 * Sets the specId.
	 *
	 * @param specId
	 *            the specId to set
	 */
	public void setSpecId(long spec) {
		this.specId = spec;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
}
