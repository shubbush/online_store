package ru.lanit.shubanev.model.product;


/**
 * characteristic class.
 *
 * @author shubbush
 */
public class Spec {

	/** The category. */
	private long category;

	/** The code. */
	private long id;

	/** The title. */
	private String title;

	/**
	 * Instantiates a new spec.
	 *
	 * @param category
	 *            the category
	 * @param code
	 *            the code
	 * @param title
	 *            the title
	 */
	public Spec(long category, long code, String title) {
		super();
		this.category = category;
		this.id = code;
		this.title = title;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public long getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category
	 *            the category to set
	 */
	public void setCategory(int category) {
		this.category = category;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public long getCode() {
		return id;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the code to set
	 */
	public void setCode(long code) {
		this.id = code;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
