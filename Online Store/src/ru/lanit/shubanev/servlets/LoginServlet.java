package ru.lanit.shubanev.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ru.lanit.shubanev.controller.command.Command;
import ru.lanit.shubanev.controller.command.ExecuteArgument;
import ru.lanit.shubanev.controller.command.SignInCommand;
import ru.lanit.shubanev.exceptions.OnlineStoreException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.user.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Map<ExecuteArgument, Object> map = new HashMap<>();
		String user = request.getParameter("user");
		map.put(ExecuteArgument.login, user);
		map.put(ExecuteArgument.password, request.getParameter("pwd"));
		Command login = new SignInCommand();
		try {
			login.execute(map);
			HttpSession session = request.getSession();
			User resUser = (User) map.get(ExecuteArgument.result);
			// User resUser = new User(3, "thom", "111");
			if (session.getAttribute("error") != null)
				session.removeAttribute("error");
			session.setAttribute("user", resUser);
			response.sendRedirect(request.getContextPath()
					+ "/profile/profile.jsp");
		} catch (UserNotFoundException e) {
			log("User not found");
			request.getSession().setAttribute("error", e.getMessage());
			response.sendRedirect("login.jsp");
			// RequestDispatcher rd = getServletContext().getRequestDispatcher(
			// "/login.jsp");
			// PrintWriter out = response.getWriter();
			// rd.include(request, response);
		} catch (OnlineStoreException e) {
			e.printStackTrace();
		}
	}
}
