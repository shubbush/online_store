package ru.lanit.shubanev.dao.dbdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private final static String HOST_NAME = "jdbc:postgresql://pg.sweb.ru:5432/oasisstoru_store";
	private final static String USERNAME = "oasisstoru_store";
	private final static String PASSWORD = "341256";
	private static boolean flag = true;

	public static Connection getConnection() throws SQLException {
		try {
			registerDriver();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return DriverManager.getConnection(HOST_NAME, USERNAME, PASSWORD);
	}

	private static void registerDriver() throws ClassNotFoundException {
		if (flag) {
			Class.forName("org.postgresql.Driver");
			flag = false;
		}
	}
}
