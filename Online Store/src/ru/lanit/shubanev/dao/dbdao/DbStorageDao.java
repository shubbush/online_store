package ru.lanit.shubanev.dao.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.logistic.Storage;

public class DbStorageDao implements StorageDao {

	@Override
	public void addLogistic(long productId, long storageId, int quantity) {
		String query = "INSERT INTO public.logistics (product_id, storage_id, quantity) "
				+ "VALUES (?, ?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, productId);
			preparedStatement.setLong(2, storageId);
			preparedStatement.setInt(3, quantity);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public void checkoutProduct(Logistic logistic, long userId)
			throws ProductQuantityInsufficientlyException {
		String checkout = "UPDATE public.logistics"
				+ "SET quantity = (logistics.quantity - carts.quantity)"
				+ "FROM public.carts  " + "WHERE carts.user_id = ?";
		String clearCart = "DELETE * FROM public.carts WHERE carts.user_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(checkout);
			preparedStatement.setLong(1, userId);
			preparedStatement.executeUpdate();
			preparedStatement = connection.prepareStatement(clearCart);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public void addStorage(String title, int deliveryTime) {
		String query = "INSERT INTO public.storages (title, delivery_time) VALUES (?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			preparedStatement.setInt(2, deliveryTime);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public int getDeliveryTimeById(long storageId)
			throws StorageNotFoundException {
		int deliveryTime = -1;
		String getTime = "SELECT storages.delivery_time "
				+ "FROM public.storages " + "WHERE storages.storage_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getTime);
			preparedStatement.setLong(1, storageId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				deliveryTime = resultSet.getInt("delivery_time");
			} else {
				throw new StorageNotFoundException();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return deliveryTime;
	}

	@Override
	public int getProductCount(long productId) {
		int productCount = 0;
		String count = "SELECT sum(logistics.quantity) as sum "
				+ "FROM public.logistics " + "WHERE logistics.product_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(count);
			preparedStatement.setLong(1, productId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				productCount = resultSet.getInt("sum");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return productCount;
	}

	@Override
	public long getStorageIdByTitle(String title) {
		long productId = -1;
		String getId = "SELECT storages.storage_id " + "FROM public.storages "
				+ "WHERE title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getId);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				productId = resultSet.getInt("storage_id");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return productId;
	}

	@Override
	public List<Logistic> getUserLogistic(long productId, int quantity) {
		List<Logistic> tmpLogistics = new ArrayList<>();
		List<Logistic> resultLogistics = new ArrayList<>();
		String query = "SELECT logistics.storage_id, logistics.quantity, storages.delivery_time "
				+ "FROM public.logistics "
				+ "INNER JOIN public.storages ON logistics.storage_id = storages.storage_id"
				+ "WHERE product_id = ? ORDER BY storages.delivery_time ASC";
		long storageId = -1;
		int tmpQuantity = quantity;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, productId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				storageId = resultSet.getLong("storage_id");
				tmpQuantity = resultSet.getInt("quantity");
				tmpLogistics
						.add(new Logistic(storageId, productId, tmpQuantity));
			}
			for (Logistic log : tmpLogistics) {
				if (tmpQuantity != 0) {
					if (log.getQuantity() > tmpQuantity) {
						resultLogistics.add(new Logistic(log.getStorageId(),
								productId, tmpQuantity));
						tmpQuantity = 0;
						break;
					} else {
						resultLogistics.add(new Logistic(log.getStorageId(),
								productId, log.getQuantity()));
						tmpQuantity -= log.getQuantity();
					}
				} else {
					break;
				}
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}

		return resultLogistics;
	}

	@Override
	public List<Storage> getStorages() {
		List<Storage> storagesList = new ArrayList<>();
		String storages = "SELECT * " + "FROM public.storages ";
		int deliveryTime = -1;
		long storageId = -1;
		String title = "";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(storages);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				deliveryTime = resultSet.getInt("delivety_time");
				storageId = resultSet.getLong("storage_id");
				title = resultSet.getString("title");
				storagesList.add(new Storage(deliveryTime, storageId, title));
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}

		return storagesList;
	}

	@Override
	public void deleteProductLogistics(long productId) {
		String query = "DELETE FROM public.carts "
				+ "WHERE carts.logistic_id  "
				+ "IN (SELECT logistics.logistic_id "
				+ "FROM public.logistics "
				+ "WHERE logistics.product_id = 1); "
				+ "DELETE FROM public.logistics WHERE logistics.product_id = ?;";

		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

}
