package ru.lanit.shubanev.dao.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.lanit.shubanev.controller.command.OrderSort;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.product.Spec;

public class DbProductDao implements ProductDAO {

	@Override
	public void addSpec(long categoryId, String title) {
		String query = "INSERT INTO public.specifics (category_id, title) VALUES (?,?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, categoryId);
			preparedStatement.setString(2, title);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public Spec getSpec(long id) {
		Spec specific = null;
		String query = "SELECT specifics.title, specifics.category_id "
				+ "FROM public.specifics " + "WHERE specifics.specific_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				String title = resultSet.getString("title");
				long categoryId = resultSet.getLong("category_id");
				specific = new Spec(categoryId, id, title);
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return specific;
	}

	@Override
	public long getSpecCategory(long specId) {
		long categoryId = -1;
		String query = "SELECT specifics.category_id "
				+ "FROM public.specifics " + "WHERE specifics.specific_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, specId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				categoryId = resultSet.getLong("category_id");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return categoryId;
	}

	@Override
	public double getPriceByCode(String code) {
		double price = -1.0;
		String query = "SELECT products.price " + "FROM public.products "
				+ "WHERE products.code = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, code);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				price = resultSet.getDouble("price");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return price;
	}

	@Override
	public void addProduct(String title, double price, String desc, String code) {
		String query = "INSERT INTO public.products (title, price, description, code) VALUES (?, ?, ?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			preparedStatement.setDouble(2, price);
			preparedStatement.setString(3, desc);
			preparedStatement.setString(4, code);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public long getCategoryId(String title) {
		long categoryId = -1;
		String query = "SELECT categories.category_id "
				+ "FROM public.categories " + "WHERE categories.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				categoryId = resultSet.getLong("category_id");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return categoryId;
	}

	@Override
	public void addCategory(String title, long parentId) {
		String query = "INSERT INTO public.categories (title, parent_id) VALUES (?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			if (parentId != 0) {
				preparedStatement.setLong(2, parentId);
			} else {
				preparedStatement.setNull(2, Types.BIGINT);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public Product getProductById(long id) throws ProductNotFoundException {
		Product product = null;
		String query = "SELECT * " + "FROM public.products "
				+ "WHERE products.product_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				String title = resultSet.getString("title");
				double price = resultSet.getDouble("price");
				String desc = resultSet.getString("description");
				String code = resultSet.getString("code");
				product = new Product(title, id, price, desc, code);
			} else {
				throw new ProductNotFoundException();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public long getProductIdByTitle(String title)
			throws ProductNotFoundException {
		long productId = -1;
		String query = "SELECT products.product_id " + "FROM public.products "
				+ "WHERE products.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				productId = resultSet.getLong("product_id");
			} else {
				throw new ProductNotFoundException();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return productId;

	}

	@Override
	public List<Spec> getCategorySpecifications(long categoryId) {
		List<Spec> specs = new ArrayList<>();
		String title = "";
		long specId = -1;
		String query = "SELECT specifics.title, specifics.specific_id "
				+ "FROM public.specifics " + "WHERE specifics.category_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, categoryId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				title = resultSet.getString("title");
				specId = resultSet.getLong("specific_id");
				specs.add(new Spec(categoryId, specId, title));
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return specs;

	}

	@Override
	public void addProductSpec(long productId, long specId, String value) {
		String query = "INSERT INTO public.product_specifics (specific_id, product_id, specific_value) VALUES (?, ?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, specId);
			preparedStatement.setLong(2, productId);
			preparedStatement.setString(3, value);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public String categoriesToString() {
		StringBuilder result = new StringBuilder();
		String query = "SELECT * " + "FROM public.categories ";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.append(resultSet.getLong("category_id") + ". ");
				result.append(resultSet.getString("title") + "\n");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return result.toString();
	}

	@Override
	public Product getProductByCode(String code) {
		Product product = null;
		String query = "SELECT * " + "FROM public.products "
				+ "WHERE products.code = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, code);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				String title = resultSet.getString("title");
				double price = resultSet.getDouble("price");
				String desc = resultSet.getString("description");
				long productId = resultSet.getLong("product_id");
				product = new Product(title, productId, price, desc, code);
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public long getSpecIdByTitle(String title) {
		long specId = -1;
		String query = "SELECT specifics.specific_id "
				+ "FROM public.specifics " + "WHERE specifics.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				specId = resultSet.getLong("specific_id");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return specId;
	}

	@Override
	public void deleteProduct(long productId) throws ProductNotFoundException {
		String query = "DELETE FROM public.products "
				+ "WHERE products.product_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, productId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public Product[] getPage(int currentElement, int count,
			Map<SortFilter, Object> map) throws ItemNotFoundException {
		List<Product> resultList = new ArrayList<>();
		String request = applyFilters(map);
		String page = request + " LIMIT ? OFFSET ?";
		int productCount = getItemsCountForRequest(map);
		int thiscount = count;
		long productId = -1;
		Product product = null;
		if (count + currentElement > productCount)
			if (productCount < 10)
				thiscount = productCount;
			else {
				thiscount = Math.abs(productCount - count - currentElement);
			}
		Product[] array = new Product[thiscount];
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(page);
			preparedStatement.setInt(1, thiscount);
			preparedStatement.setInt(2, currentElement);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				productId = resultSet.getLong(1);
				if (productId == 0) {
					productId = resultSet.getLong(2);
				}
				product = getProductById(productId);
				resultList.add(product);
			}
		} catch (ProductNotFoundException e) {
			System.err.println("product not found");
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return resultList.toArray(array);

	}

	@Override
	public int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		int count = -1;
		String main = applyFilters(map);
		String start = "SELECT count(*) from (";
		String end = ") AS count";
		String result = start + main + end;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(result);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt("count");
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return count;
	}

	private String applyFilters(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		String first = "SELECT * FROM (SELECT products.product_id from public.products where products.title like '%";
		String second = "%' or products.description like '%";
		String third = "%') as one FULL join (select product_specifics.product_id "
				+ "from public.product_specifics where product_specifics.specific_value like '%";
		String fourth = "%') as two on one.product_id = two.product_id";

		String main = "SELECT * FROM public.products ";
		String where = "WHERE ";
		String sorting = "ORDER BY ";
		String highToLow = "DESC ";
		String lowToHigh = "ASC ";
		String resultQuery = main;
		for (Map.Entry<SortFilter, Object> entity : map.entrySet()) {
			switch (entity.getKey()) {
			case searchString:
				String str = (String) entity.getValue();
				resultQuery = first + str + second + str + third + str + fourth;
				break;
			case sortByCategory:
				// loh
				break;
			case sortByPrice:
				resultQuery += sorting + "price ";
				if (((OrderSort) entity.getValue()).equals(OrderSort.lowTohigh)) {
					resultQuery += lowToHigh;
				} else {
					resultQuery += highToLow;
				}
				break;
			case sortByTitle:
				resultQuery += sorting + "title ";
				if (((OrderSort) entity.getValue()).equals(OrderSort.lowTohigh)) {
					resultQuery += lowToHigh;
				} else {
					resultQuery += highToLow;
				}
				break;
			default:
				break;
			}
		}
		return resultQuery;
	}
}
