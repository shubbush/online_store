package ru.lanit.shubanev.dao.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AccessDao;
import ru.lanit.shubanev.model.access.Permission;
import ru.lanit.shubanev.model.access.Role;
import ru.lanit.shubanev.model.access.RolePermission;
import ru.lanit.shubanev.model.access.UserRole;

public class DbAccessDao implements AccessDao {

	@Override
	public void addUserRole(UserRole userRole) {
		String addUserRole = "INSERT INTO public.user_roles (user_id, role_id) VALUES (?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(addUserRole);
			preparedStatement.setLong(1, userRole.getUserId());
			preparedStatement.setLong(2, userRole.getRoleId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public void addRole(String title) {
		String addRole = "INSERT INTO public.roles (title) VALUES (?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(addRole);
			preparedStatement.setString(1, title);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public void addRolePermission(RolePermission rolePermission) {
		String addRolePermission = "INSERT INTO public.role_permissions (role_id, permission_id) VALUES (?, ?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(addRolePermission);
			preparedStatement.setLong(1, rolePermission.getRoleId());
			preparedStatement.setLong(2, rolePermission.getPermissionId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public Role getRoleByTitle(String title) {
		Role role = null;
		long roleId = -1;
		String getRole = "SELECT roles.role_id FROM public.roles WHERE roles.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getRole);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				roleId = resultSet.getLong("role_id");
				role = new Role(roleId, title);
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return role;
	}

	@Override
	public Permission getPermissionByTitle(String title) {
		Permission permission = null;
		long permissionId = -1;
		String getRole = "SELECT permissions.permission_id FROM public.permissions WHERE permissions.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getRole);
			preparedStatement.setString(1, title);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				permissionId = resultSet.getLong("permission_id");
				permission = new Permission(permissionId, title);
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return permission;
	}

	@Override
	public long getPermissionId(String str) {
		long permissionId = -1;
		String getPermission = "SELECT permissions.permission_id FROM public.permissions WHERE permissions.title = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getPermission);
			preparedStatement.setString(1, str);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				permissionId = resultSet.getLong("permission_id");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return permissionId;
	}

	@Override
	public List<Long> getRolePemissions(long roleId) {
		List<Long> permissions = new ArrayList<>();
		String query = "SELECT role_permissions.permission_id "
				+ "FROM public.role_permissions "
				+ "WHERE role_permissions.role_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, roleId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				permissions.add(resultSet.getLong("permission_id"));
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return permissions;
	}

	@Override
	public String getPermissionStringById(long permission) {
		String permissionTitle = "";
		String getPermissionTitle = "SELECT permissions.title "
				+ "FROM public.permissions "
				+ "WHERE permissions.permission_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getPermissionTitle);
			preparedStatement.setLong(1, permission);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				permissionTitle = resultSet.getString("title");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return permissionTitle;
	}

	@Override
	public void addPermission(String title) {
		String addPermission = "INSERT INTO public.permissions (title) VALUES (?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(addPermission);
			preparedStatement.setString(1, title);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public List<Long> getUserRoles(long userId) {
		List<Long> roles = new ArrayList<>();
		String query = "SELECT user_roles.role_id " + "FROM public.user_roles "
				+ "WHERE user_roles.user_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(query);
			preparedStatement.setLong(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				roles.add(resultSet.getLong("role_id"));
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public String getRoleStringById(long roleId) {
		String role = "";
		String getRoleStringById = "SELECT roles.title " + "FROM public.roles "
				+ "WHERE roles.role_id = ?";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(getRoleStringById);
			preparedStatement.setLong(1, roleId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				role = resultSet.getString("title");
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return role;
	}

}
