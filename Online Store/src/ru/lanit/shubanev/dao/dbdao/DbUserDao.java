package ru.lanit.shubanev.dao.dbdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.lanit.shubanev.controller.command.OrderSort;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.user.Cart;
import ru.lanit.shubanev.model.user.User;

public class DbUserDao implements UserDAO {

	@Override
	public User[] getPage(int currentElement, int count,
			Map<SortFilter, Object> map) throws ItemNotFoundException {
		List<User> resultList = new ArrayList<>();
		String request = applyFilters(map);
		String page = request + "LIMIT ? OFFSET ?";
		int usersCount = getItemsCountForRequest(map);
		int thiscount = count;
		String login = "";
		String password = "";
		long userId = -1;
		if (count + currentElement > usersCount)
			if (usersCount < 10)
				thiscount = usersCount;
			else {
				thiscount = Math.abs(usersCount - count - currentElement);
			}
		User[] array = new User[thiscount];
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(page);
			preparedStatement.setInt(1, thiscount);
			preparedStatement.setInt(2, currentElement);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				login = resultSet.getString("login");
				password = resultSet.getString("password");
				userId = resultSet.getLong("user_id");
				resultList.add(new User(userId, login, password));
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return resultList.toArray(array);
	}

	@Override
	public int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		int count = -1;
		String main = applyFilters(map);
		String start = "SELECT count(user_id) from (";
		String end = ") AS count";
		String result = start + main + end;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement(result);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			count = resultSet.getInt("count");
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return count;
	}

	private String applyFilters(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		String sortByLogin = "SELECT users.user_id, users.login, users.password FROM public.users ";
		String where = "WHERE login = ? ";
		String sorting = "ORDER BY login ";
		String highToLow = "DESC ";
		String lowToHigh = "ASC ";
		String resultQuery = "";
		for (Map.Entry<SortFilter, Object> entity : map.entrySet()) {
			switch (entity.getKey()) {
			case sortByLogin:
				resultQuery += sortByLogin + sorting;
				if (((OrderSort) entity.getValue()).equals(OrderSort.lowTohigh)) {
					resultQuery += lowToHigh;
				} else {
					resultQuery += highToLow;
				}
				break;
			default:
				break;
			}
		}
		return resultQuery;
	}

	@Override
	public void addCart(Cart cart) {
		long userId = cart.getUserId();
		long logisticId = -1;
		String insertLogistic = "INSERT INTO public.logistics (product_id, storage_id, quantity) "
				+ "VALUES (?, ?, ?) " + "RETURNING logistic_id";
		String insertCart = "INSERT INTO public.carts (user_id, logistic_id) VALUES (?,?)";
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = null;
			for (Logistic logistic : cart.getCart()) {
				preparedStatement = connection.prepareStatement(insertLogistic);
				preparedStatement.setLong(1, logistic.getProductId());
				preparedStatement.setLong(2, logistic.getStorageId());
				preparedStatement.setInt(3, logistic.getQuantity());
				ResultSet resultSet = preparedStatement.executeQuery();
				resultSet.next();
				logisticId = resultSet.getLong("logistic_id");
				preparedStatement = connection.prepareStatement(insertCart);
				preparedStatement.setLong(1, userId);
				preparedStatement.setLong(1, logisticId);
				preparedStatement.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public void addUser(String login, String password) {
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO public.users (login, password) "
							+ "VALUES (?, ?)");
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

	@Override
	public Cart getUserCart(long userId) {
		Cart cart = null;
		List<Logistic> logistics = new ArrayList<>();
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT logistics.product_id, logistics.storage_id, logistics.quantity "
							+ "FROM public.carts INNER JOIN public.logistics ON carts.logistic_id = logistics.logistic_id "
							+ "WHERE carts.user_id = ?");
			preparedStatement.setLong(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				long productId = resultSet.getLong("product_id");
				long storageId = resultSet.getLong("storage_id");
				int quantity = resultSet.getInt("quantity");
				Logistic logistic = new Logistic(storageId, productId, quantity);
				logistics.add(logistic);
			}
			cart = new Cart(userId, logistics);
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return cart;
	}

	@Override
	public User getUserByLogin(String login) {
		User user = null;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT users.user_id, users.login, users.password "
							+ "FROM public.users " + "WHERE users.login = ?");
			preparedStatement.setString(1, login);
			ResultSet result = preparedStatement.executeQuery();
			result.next();
			long userId = result.getLong("user_id");
			String password = result.getString("password");
			user = new User(userId, login, password);
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public long getUserIdByLogin(String login) {
		long userId = -1;
		PreparedStatement preparedStatement;
		try (Connection connection = DBConnection.getConnection()) {
			preparedStatement = connection
					.prepareStatement("SELECT users.user_id "
							+ "FROM public.users " + "WHERE users.login = ?");
			preparedStatement.setString(1, login);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next())
				userId = result.getLong("user_id");
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return userId;
	}

	@Override
	public User getUserById(long id) throws UserNotFoundException {
		User user = null;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT users.user_id, users.login, users.password "
							+ "FROM public.users " + "WHERE users.user_id = ?");
			preparedStatement.setLong(1, id);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next()) {
				long userId = result.getLong("user_id");
				String login = result.getString("login");
				String password = result.getString("password");
				user = new User(userId, login, password);
			} else {
				throw new UserNotFoundException();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User signIn(String login, String password)
			throws UserNotFoundException {
		User user = null;
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT users.user_id "
							+ "FROM public.users "
							+ "WHERE users.login = ? AND users.password = ?");
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next()) {
				long userId = result.getLong("user_id");
				user = new User(userId, login, password);
			} else {
				throw new UserNotFoundException(login);
			}
		} catch (SQLException e) {
			System.out.println("creating connection failed");
			e.printStackTrace();
		}
		return user;

	}

	@Override
	public void addProductToUserCart(List<Logistic> logistics, long userId) {
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO public.carts "
							+ "(user_id, product_id, storage_id, quantity) VALUES (?, ?, ?, ?");
			for (Logistic logistic : logistics) {
				preparedStatement.setLong(1, userId);
				preparedStatement.setLong(2, logistic.getProductId());
				preparedStatement.setLong(3, logistic.getStorageId());
				preparedStatement.setInt(4, logistic.getQuantity());
				preparedStatement.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println("creaing connection failed");
			e.printStackTrace();
		}
	}

}
