package ru.lanit.shubanev.dao.dbdao;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.AccessDao;
import ru.lanit.shubanev.dao.abstractdao.PagedDao;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.model.ListItem;

public class DbDaoFactory extends AbstractDAOFactory {

	@Override
	public ProductDAO getProductDAO() {
		return new DbProductDao();
	}

	@Override
	public UserDAO getUserDAO() {
		return new DbUserDao();
	}

	@Override
	public StorageDao getStorageDao() {
		return new DbStorageDao();
	}

	@Override
	public AccessDao getAccessDao() {
		return new DbAccessDao();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends ListItem> PagedDao<T> getPagedDao(Class<T> clazz) {
		String str = clazz.getName();
		switch (str) {
		case "ru.lanit.shubanev.model.product.Product": {
			return (PagedDao<T>) new DbProductDao();
		}
		}
		return (PagedDao<T>) new DbUserDao();
	}

}
