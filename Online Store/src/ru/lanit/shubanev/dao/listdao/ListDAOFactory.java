package ru.lanit.shubanev.dao.listdao;

import ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory;
import ru.lanit.shubanev.dao.abstractdao.AccessDao;
import ru.lanit.shubanev.dao.abstractdao.PagedDao;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.model.ListItem;

/**
 * A factory for creating ListDAO objects.
 */
public class ListDAOFactory extends AbstractDAOFactory {

	/**
	 * Gets the product dao.
	 *
	 * @return the product dao
	 * @see ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory#getProductDAO()
	 */
	@Override
	public ProductDAO getProductDAO() {
		return new ListProductDAO();
	}

	/**
	 * Gets the user dao.
	 *
	 * @return the user dao
	 * @see ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory#getUserDAO()
	 */
	@Override
	public UserDAO getUserDAO() {
		return new ListUserDAO();
	}

	/**
	 * Gets the storage dao.
	 *
	 * @return the storage dao
	 * @see ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory#getStorageDao()
	 */
	@Override
	public StorageDao getStorageDao() {
		return new ListStorageDao();
	}

	/**
	 * @see ru.lanit.shubanev.dao.abstractdao.AbstractDAOFactory#getAccessDao()
	 */
	@Override
	public AccessDao getAccessDao() {
		return new ListAccessDao();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends ListItem> PagedDao<T> getPagedDao(Class<T> clazz) {
		String str = clazz.getName();

		switch (str) {
		case "ru.lanit.shubanev.model.product.Product": {
			return (PagedDao<T>) new ListProductDAO();
		}
		}
		return (PagedDao<T>) new ListUserDAO();
	}
}
