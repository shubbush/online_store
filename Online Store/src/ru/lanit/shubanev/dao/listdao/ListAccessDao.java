package ru.lanit.shubanev.dao.listdao;

import java.util.ArrayList;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.AccessDao;
import ru.lanit.shubanev.model.access.Permission;
import ru.lanit.shubanev.model.access.Role;
import ru.lanit.shubanev.model.access.RolePermission;
import ru.lanit.shubanev.model.access.UserRole;
import ru.lanit.shubanev.tools.Incrementer;

/**
 * The Class ListAccessDao.
 */
public class ListAccessDao implements AccessDao {

	/** Roles. */
	private static List<Role> roles = new ArrayList<>();

	/** Role & command. */
	private static List<RolePermission> rolesPermissions = new ArrayList<>();

	/** Users & Roles. */
	private static List<UserRole> usersRoles = new ArrayList<>();

	/** The permissions. */
	private static List<Permission> permissions = new ArrayList<>();

	public final static Incrementer ROLE_INCREMENTER = new Incrementer();
	public final static Incrementer PERMISSION_INCREMENTER = new Incrementer();

	/**
	 * Constructor.
	 */
	public ListAccessDao() {

	}

	@Override
	public String getPermissionStringById(long permission) {
		String str = "";

		for (Permission permis : permissions) {
			// System.out.println(perstr.getString());
			if (permis.getId() == permission) {
				str = permis.getPermission();
			}
		}
		return str;
	}

	@Override
	public long getPermissionId(String str) {
		long id = -1;
		for (Permission permission : permissions) {
			if (permission.getPermission().equals(str))
				id = permission.getId();
		}
		return id;
	}

	/**
	 * @see ru.lanit.shubanev.dao.abstractdao.AccessDao#addPermission(ru.lanit.shubanev.model.access.Permission)
	 */
	@Override
	public void addPermission(String title) {
		Permission permission = new Permission(
				PERMISSION_INCREMENTER.getNextId(), title);
		permissions.add(permission);
	}

	/**
	 * @see ru.lanit.shubanev.dao.abstractdao.AccessDao#addUserRole(ru.lanit.shubanev.model.access.UserRole)
	 */
	@Override
	public void addUserRole(UserRole userRole) {
		usersRoles.add(userRole);
	}

	/**
	 * @see ru.lanit.shubanev.dao.abstractdao.AccessDao#addRole(ru.lanit.shubanev.model.access.Role)
	 */
	@Override
	public void addRole(String title) {
		Role role = new Role(ROLE_INCREMENTER.getNextId(), title);
		roles.add(role);
	}

	/**
	 * @see ru.lanit.shubanev.dao.abstractdao.AccessDao#addRolePermission(ru.lanit.shubanev.model.access.RolePermission)
	 */
	@Override
	public void addRolePermission(RolePermission rolePermission) {
		rolesPermissions.add(rolePermission);
	}

	@Override
	public Role getRoleByTitle(String title) {
		for (Role role : roles)
			if (role.getTitle().equals(title))
				return role;
		return null;
	}

	@Override
	public Permission getPermissionByTitle(String title) {
		for (Permission permission : permissions) {
			if (permission.getPermission().equals(title))
				return permission;
		}
		return null;
	}

	@Override
	public List<Long> getRolePemissions(long roleId) {
		List<Long> permissions = new ArrayList<>();
		for (RolePermission rolePermission : rolesPermissions) {
			if (rolePermission.getRoleId() == roleId) {
				permissions.add(rolePermission.getPermissionId());
			}
		}
		return permissions;
	}

	@Override
	public List<Long> getUserRoles(long userId) {
		List<Long> userRoles = new ArrayList<>();
		for (UserRole userRole : usersRoles) {
			if (userRole.getUserId() == userId) {
				userRoles.add(userRole.getRoleId());
			}
		}
		return userRoles;
	}

	@Override
	public String getRoleStringById(long roleId) {
		String roleTitle = "";
		for (Role role : roles) {
			if (role.getId() == roleId) {
				roleTitle = role.getTitle();
				break;
			}
		}
		return roleTitle;
	}
}
