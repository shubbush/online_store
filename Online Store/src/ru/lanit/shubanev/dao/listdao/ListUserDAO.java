package ru.lanit.shubanev.dao.listdao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ru.lanit.shubanev.controller.command.OrderSort;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.dao.abstractdao.UserDAO;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.user.Cart;
import ru.lanit.shubanev.model.user.User;
import ru.lanit.shubanev.tools.Incrementer;
import ru.lanit.shubanev.tools.UserLoginComparator;

/**
 * The Class ListUserDAO.
 */
public class ListUserDAO implements UserDAO {

	/** User incrementer */
	private final static Incrementer USER_INCREMENTER = new Incrementer();

	/** The users. */
	private static List<User> users = new ArrayList<>();

	/** The carts. */
	private static List<Cart> carts = new ArrayList<>();

	/**
	 * Adds the cart.
	 *
	 * @param cart
	 *            the cart
	 * @see ru.lanit.shubanev.dao.abstractdao.UserDAO#addCart(ru.lanit.shubanev.model.user.Cart)
	 */
	@Override
	public void addCart(Cart cart) {
		ListUserDAO.carts.add(cart);
	}

	/**
	 * Instantiates a new list user dao.
	 */
	public ListUserDAO() {
	}

	/**
	 * Создает аккаунт сотрудника магазина, хэширует пароль и записывает в
	 * Users.
	 *
	 * @param user
	 *            the user
	 */
	@Override
	public void addUser(String login, String password) {
		User user = new User(USER_INCREMENTER.getNextId(), login, password);
		users.add(user);
	}

	@Override
	public User getUserByLogin(String login) {
		for (User user : users)
			if (user.getLogin().equals(login))
				return user;
		return null;
	}

	@Override
	public User[] getPage(int currentElement, int count,
			Map<SortFilter, Object> map) throws ItemNotFoundException {
		int thiscount = count;
		List<User> users = applyFilters(map);
		if (count + currentElement > users.size())
			if (users.size() < 10)
				thiscount = users.size();
			else {
				thiscount = Math.abs(users.size() - count - currentElement);
			}
		System.out.println("thiscount " + thiscount);
		User[] array = new User[thiscount];
		return users.subList(currentElement, thiscount + currentElement)
				.toArray(array);
	}

	@Override
	public int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		return applyFilters(map).size();
	}

	private List<User> applyFilters(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		List<User> resultList = new ArrayList<>();
		resultList.addAll(users);
		for (Map.Entry<SortFilter, Object> entity : map.entrySet()) {
			switch (entity.getKey()) {
			case sortByLogin:
				if (entity.getValue() instanceof OrderSort) {
					Collections.sort(resultList, new UserLoginComparator());
					if (((OrderSort) entity.getValue())
							.equals(OrderSort.highToLow))
						Collections.reverse(resultList);
				}
				break;
			default:
				break;
			}
		}
		return resultList;
	}

	@Override
	public User getUserById(long id) throws UserNotFoundException {
		User user = null;
		for (User uzer : users) {
			if (uzer.getId() == id)
				user = uzer;
		}
		if (user == null)
			throw new UserNotFoundException();
		return user;
	}

	@Override
	public long getUserIdByLogin(String login) {
		return getUserByLogin(login).getId();
	}

	@Override
	public Cart getUserCart(long userId) {
		Cart userCart = null;
		for (Cart cart : carts) {
			if (cart.getUserId() == userId) {
				userCart = cart;
				break;
			}
		}
		return userCart;
	}

	@Override
	public User signIn(String login, String password)
			throws UserNotFoundException {
		User user = null;
		for (User us : users) {
			if (us.getLogin().equals(login)
					&& us.getPassword().equals(password)) {
				user = us;
			}
		}
		if (user == null) {
			throw new UserNotFoundException(login);
		}
		return user;
	}

	@Override
	public void addProductToUserCart(List<Logistic> logistic, long userId) {
		for (Cart cart : carts) {
			if (cart.getUserId() == userId) {
				cart.getCart().addAll(logistic);
			}
		}
	}
}
