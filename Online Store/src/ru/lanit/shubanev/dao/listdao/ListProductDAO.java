package ru.lanit.shubanev.dao.listdao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.lanit.shubanev.controller.command.OrderSort;
import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.dao.abstractdao.ProductDAO;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.product.Category;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.product.ProductSpec;
import ru.lanit.shubanev.model.product.Spec;
import ru.lanit.shubanev.tools.Incrementer;
import ru.lanit.shubanev.tools.ProductPriceComparator;
import ru.lanit.shubanev.tools.ProductTitleComparator;

/**
 * The Class ListProductDAO.
 */
public class ListProductDAO implements ProductDAO {

	/** The incr categories. */
	public static final Incrementer INCRIMENT_CATEGORIES = new Incrementer();
	/** The incr prod. */
	public static final Incrementer INCRIMENT_PRODUCT = new Incrementer();
	/** The incr specs. */
	public static final Incrementer INCRIMENT_SPECIFICATIONS = new Incrementer();

	/** The products. */
	private static List<Product> products = new ArrayList<>();

	/** The categories. */
	private static List<Category> categories = new ArrayList<>();

	/** The specs. */
	private static List<Spec> specs = new ArrayList<>();

	/** The specs of products **/
	private static List<ProductSpec> productSpecs = new ArrayList<>();

	/**
	 * Instantiates a new list product dao.
	 */
	public ListProductDAO() {
	}

	/**
	 * Adds the spec.
	 *
	 * @param spec
	 *            the spec
	 * @see ru.lanit.shubanev.dao.abstractdao.ProductDAO#addSpec(int,
	 *      java.lang.String)
	 */
	@Override
	public void addSpec(long categoryId, String title) {
		Spec spec = new Spec(categoryId, INCRIMENT_SPECIFICATIONS.getNextId(),
				title);
		ListProductDAO.specs.add(spec);
	}

	/**
	 * Gets the spec.
	 *
	 * @param title
	 *            the title
	 * @return the spec
	 * @see ru.lanit.shubanev.dao.abstractdao.ProductDAO#getSpec(java.lang.String)
	 */
	@Override
	public Spec getSpec(long id) {
		Spec tmpSpec = null;
		for (Spec spec : ListProductDAO.specs) {
			if (spec.getCode() == id) {
				tmpSpec = spec;
			}
		}
		return tmpSpec;
	}

	/**
	 * Gets the spec category.
	 *
	 * @param specId
	 *            the spec id
	 * @return the spec category
	 * @see ru.lanit.shubanev.dao.abstractdao.ProductDAO#getSpecCategory(int)
	 */
	@Override
	public long getSpecCategory(long specId) {
		for (Spec spec : ListProductDAO.specs) {
			if (spec.getCode() == specId)
				return spec.getCategory();
		}
		return -1;
	}

	@Override
	public Product getProductById(long id) throws ProductNotFoundException {
		Product product = null;
		for (Product prod : products) {
			if (prod.getId() == id)
				product = prod;
		}
		if (product == null) {
			throw new ProductNotFoundException();
		}
		return product;
	}

	/**
	 * Check id.
	 *
	 * @param isbn
	 *            the isbn
	 * @return the double
	 * @see ru.lanit.shubanev.dao.abstractdao.ProductDAO#getPriceByCode(java.lang.String)
	 */
	@Override
	public double getPriceByCode(String isbn) {
		double tmp = -1;
		for (Product pr : ListProductDAO.products) {
			if (pr.getCode() == isbn) {
				tmp = pr.getPrice();
			}
		}
		return tmp;
	}

	/**
	 * записывает в products.
	 * 
	 * List of specification of product
	 *
	 * @param product
	 *            the product
	 */
	@Override
	public void addProduct(String title, double price, String desc, String code) {
		Product product = new Product(title, INCRIMENT_PRODUCT.getNextId(),
				price, desc, code);
		products.add(product);
	}

	/**
	 * Returns ID of category by input string.
	 *
	 * @param title
	 *            Category title
	 * @return ID Category ID
	 */
	@Override
	public long getCategoryId(String title) {
		long tmpId = -1;
		for (Category cat : ListProductDAO.categories) {
			if (cat.getTitle().equals(title)) {
				tmpId = cat.getId();
			}
		}
		return tmpId;
	}

	/**
	 * Добавляет категорию, Добавляет категорию в hashMap, перед добавлением
	 * проверяет существование категории с таким названием, при условии
	 * существования возвращается ее ID.
	 *
	 * @param category
	 *            the category
	 * @return Возвращает ID категории
	 */
	@Override
	public void addCategory(String title, long parentId) {
		Category category = new Category(INCRIMENT_CATEGORIES.getNextId(),
				title, parentId);
		categories.add(category);
	}

	/**
	 * Представление hashMap categories в виде строки.
	 *
	 * @return hashMap categories в виде строки
	 */
	@Override
	public String categoriesToString() {
		String result = "";
		Iterator<Category> itr = categories.iterator();
		if (itr.hasNext()) {
			itr.next();
		}
		while (itr.hasNext()) {
			Category entry = itr.next();
			result += entry.getId() + ". " + entry.getTitle() + "\n";
		}
		return result;
	}

	/**
	 * Gets the product by code.
	 *
	 * @param code
	 *            the code
	 * @return the product by code
	 * @see ru.lanit.shubanev.dao.abstractdao.ProductDAO#getProductByCode(java.lang.String)
	 */
	@Override
	public Product getProductByCode(String code) {
		for (Product prod : ListProductDAO.products) {
			if (prod.getCode().equals(code)) {
				return prod;
			}
		}
		return null;
	}

	@Override
	public Product[] getPage(int currentElement, int count,
			Map<SortFilter, Object> map) throws ItemNotFoundException {
		int thiscount = count;
		List<Product> products = applyFilters(map);
		if (products.size() < 10)
			thiscount = products.size();
		else {
			thiscount = Math.abs(products.size() - count - currentElement);
		}
		Product[] array = new Product[thiscount];
		return products.subList(currentElement, thiscount + currentElement)
				.toArray(array);
	}

	/**
	 * Ищет объекты книг, содержащие подстроку в title или author .
	 *
	 * @param request
	 *            поисковый запрос
	 * @return ArrayList, состоящий из книг, найденных по вхождению
	 * @throws ItemNotFoundException
	 *             the product not found exception
	 */
	private List<Product> searchProduct(String request, List<Product> products)
			throws ItemNotFoundException {
		ArrayList<Product> result = new ArrayList<Product>();
		StringBuffer resStr = new StringBuffer();
		String[] requestArray = request.split(" ");
		for (Product prod : products) {
			resStr.setLength(0);
			for (ProductSpec specs : productSpecs) {
				if (specs.getProductId() == prod.getId())
					resStr.append(specs.getValue().toLowerCase() + " ");
			}
			for (int i = 0; i < requestArray.length; i++) {
				if (prod.getTitle().toLowerCase()
						.indexOf(requestArray[i].toLowerCase())
						+ resStr.indexOf(requestArray[i].toLowerCase()) >= -1) {
					result.add(prod);
				}
			}
		}
		if (result.size() == 0) {
			throw new ItemNotFoundException(request);
		}
		return result;
	}

	private List<Product> applyFilters(Map<SortFilter, Object> map)
			throws ItemNotFoundException {
		List<Product> resultList = new ArrayList<>();
		resultList.addAll(products);
		for (Map.Entry<SortFilter, Object> entity : map.entrySet()) {
			switch (entity.getKey()) {
			case searchString:
				resultList = searchProduct((String) entity.getValue(),
						resultList);
				break;
			case sortByCategory:
				if (entity.getValue() instanceof Long)
					resultList = getProductsByCategory((long) entity.getValue());
				break;
			case sortByPrice:
				if (entity.getValue() instanceof OrderSort) {
					Collections.sort(resultList, new ProductPriceComparator());
					if (((OrderSort) entity.getValue())
							.equals(OrderSort.highToLow))
						Collections.reverse(resultList);
				}
			case sortByTitle:
				if (entity.getValue() instanceof OrderSort) {
					Collections.sort(resultList, new ProductTitleComparator());
					if (((OrderSort) entity.getValue())
							.equals(OrderSort.highToLow))
						Collections.reverse(resultList);
				}

				break;
			default:
				break;
			}
		}
		return resultList;
	}

	/**
	 * Ищет объекты книг определенной категории .
	 *
	 * @param request
	 *            название категории
	 * @return ArrayList, состоящий из книг определенной категории
	 */
	private List<Product> getProductsByCategory(long request) {
		Set<Long> specsIds = new HashSet<>();
		List<Product> result = new ArrayList<Product>();
		for (Spec specy : specs) {
			if (specy.getCategory() == request)
				specsIds.add(specy.getCode());
		}
		for (Product product : products) {
			Set<Long> productSpecsId = new HashSet<>();
			long productId = product.getId();
			for (ProductSpec productSpec : productSpecs) {
				if (productSpec.getProductId() == productId)
					productSpecsId.add(productSpec.getSpecId());
			}
			productSpecsId.retainAll(specsIds);
			if (productSpecsId.size() != 0)
				result.add(product);
		}
		return result;
	}

	@Override
	public int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException {

		return applyFilters(map).size();
	}

	@Override
	public List<Spec> getCategorySpecifications(long categoryId) {
		List<Spec> specifics = new ArrayList<Spec>();
		for (Spec spec : specs) {
			if (spec.getCategory() == categoryId) {
				specifics.add(spec);
			}
		}
		return specifics;
	}

	@Override
	public long getProductIdByTitle(String title)
			throws ProductNotFoundException {
		long productId = -1;
		for (Product prod : products) {
			if (prod.getTitle() == title) {
				productId = prod.getId();
				break;
			}
		}
		if (productId == -1) {
			throw new ProductNotFoundException();
		}
		return productId;
	}

	@Override
	public void addProductSpec(long productId, long specId, String value) {
		productSpecs.add(new ProductSpec(productId, specId, value));
	}

	@Override
	public long getSpecIdByTitle(String title) {
		long specy = -1;
		for (Spec spec : specs) {
			if (spec.getTitle().equals(title))
				specy = spec.getCode();
		}
		return specy;
	}

	@Override
	public void deleteProduct(long productId) throws ProductNotFoundException {
		int index = -1;
		for (int i = 0; i < products.size(); i++) {
			if (products.get(i).getId() == productId) {
				index = i;
				break;
			}
		}
		if (index >= 0) {
			products.remove(index);
		} else {
			throw new ProductNotFoundException();
		}
	}

}
