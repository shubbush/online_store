package ru.lanit.shubanev.dao.listdao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.lanit.shubanev.dao.abstractdao.StorageDao;
import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.logistic.Storage;
import ru.lanit.shubanev.tools.Incrementer;
import ru.lanit.shubanev.tools.LogisticComparator;

/**
 * The Class ListStorageDao.
 */
public class ListStorageDao implements StorageDao {

	public static final Incrementer STORAGE_INCREMENTER = new Incrementer();

	/** The logistics. */
	static List<Logistic> logistics = new ArrayList<>();

	/** The storages. */
	private static List<Storage> storages = new ArrayList<>();

	/**
	 * Adds the logistic.
	 *
	 * @param log
	 *            the log
	 * @see ru.lanit.shubanev.dao.abstractdao.StorageDao#addLogistic(ru.lanit.shubanev.model.
	 *      logistic.Logistic)
	 */
	@Override
	public void addLogistic(long productId, long storageId, int quantity) {
		Logistic log = new Logistic(storageId, productId, quantity);
		ListStorageDao.logistics.add(log);
	}

	/**
	 * Adds the storage.
	 *
	 * @param stor
	 *            the stor
	 * @see ru.lanit.shubanev.dao.abstractdao.StorageDao#addStorage(ru.lanit.shubanev.model.logistic
	 *      .Storage)
	 */
	@Override
	public void addStorage(String title, int deliveryTime) {
		Storage stor = new Storage(deliveryTime,
				STORAGE_INCREMENTER.getNextId(), title);
		ListStorageDao.storages.add(stor);
	}

	/**
	 * Checkout product.
	 *
	 * @param logistic
	 *            the logistic
	 * @throws ProductQuantityInsufficientlyException
	 *             the product quantity insufficiently exception
	 * @see ru.lanit.shubanev.dao.abstractdao.StorageDao#checkoutProduct(ru.lanit.shubanev.model
	 *      .logistic.Logistic)
	 */
	@Override
	public void checkoutProduct(Logistic logistic, long userId)
			throws ProductQuantityInsufficientlyException {
		for (Logistic log : logistics) {
			if (log.getProductId() == logistic.getProductId()
					&& log.getStorageId() == logistic.getStorageId()) {
				if (log.getQuantity() >= logistic.getQuantity()) {
					log.setQuantity(log.getQuantity() - logistic.getQuantity());
				} else {
					throw new ProductQuantityInsufficientlyException(
							String.valueOf(logistic.getProductId()));
				}
			}
		}
	}

	@Override
	public int getDeliveryTimeById(long storageId)
			throws StorageNotFoundException {
		int deliveryTime = -1;
		for (Storage storage : storages) {
			if (storage.getId() == storageId) {
				deliveryTime = storage.getDeliveryTime();
			}
		}
		if (deliveryTime == -1)
			throw new StorageNotFoundException();
		return deliveryTime;
	}

	@Override
	public int getProductCount(long productId) {
		int count = 0;
		for (Logistic log : logistics) {
			if (log.getProductId() == productId) {
				count += log.getQuantity();
			}
		}
		return count;
	}

	@Override
	public long getStorageIdByTitle(String title) {
		long storageId = -1;
		for (Storage storage : storages) {
			if (storage.getTitle().equals(title)) {
				storageId = storage.getId();
				break;
			}
		}
		return storageId;
	}

	@Override
	public List<Logistic> getUserLogistic(long productId, int quantity) {
		List<Logistic> tmpLog = new ArrayList<>();
		List<Logistic> userLogistic = new ArrayList<>();

		for (Logistic log : logistics) {
			if (log.getProductId() == productId) {
				tmpLog.add(log);
			}
		}
		Collections.sort(tmpLog, new LogisticComparator());
		int tmpQuan = quantity;

		for (Logistic log : tmpLog) {
			if (tmpQuan != 0) {
				if (log.getQuantity() > tmpQuan) {
					userLogistic.add(new Logistic(log.getStorageId(),
							productId, tmpQuan));
					tmpQuan = 0;
				} else {
					userLogistic.add(new Logistic(log.getStorageId(),
							productId, log.getQuantity()));
					tmpQuan -= log.getQuantity();
				}
			} else {
				break;
			}
		}
		return userLogistic;
	}

	@Override
	public List<Storage> getStorages() {
		return storages;
	}

	@Override
	public void deleteProductLogistics(long productId) {
		List<Logistic> tmpLog = new ArrayList<>();
		for (Logistic logistic : logistics) {
			if (logistic.getProductId() == productId)
				tmpLog.add(logistic);
		}
		logistics.removeAll(tmpLog);
	}
}
