package ru.lanit.shubanev.dao.abstractdao;

import ru.lanit.shubanev.dao.dbdao.DbDaoFactory;
import ru.lanit.shubanev.model.ListItem;

/**
 * A factory for creating AbstractDAO objects.
 */
public abstract class AbstractDAOFactory {

	/**
	 * Gets the product dao.
	 *
	 * @return the product dao
	 */
	public abstract ProductDAO getProductDAO();

	/**
	 * Gets the user dao.
	 *
	 * @return the user dao
	 */
	public abstract UserDAO getUserDAO();

	/**
	 * Gets the storage dao.
	 *
	 * @return the storage dao
	 */
	public abstract StorageDao getStorageDao();

	/**
	 * Gets the access dao.
	 *
	 * @return the access dao
	 */
	public abstract AccessDao getAccessDao();

	public abstract <T extends ListItem> PagedDao<T> getPagedDao(Class<T> clazz);

	/**
	 * Gets the DAO factory.
	 *
	 * @return the DAO factory
	 */
	public static AbstractDAOFactory getDAOFactory() {

		return new DbDaoFactory();
	}

}
