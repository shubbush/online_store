package ru.lanit.shubanev.dao.abstractdao;

import java.util.List;

import ru.lanit.shubanev.exceptions.ProductQuantityInsufficientlyException;
import ru.lanit.shubanev.exceptions.StorageNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.logistic.Storage;

/**
 * The Interface StorageDao.
 */
public interface StorageDao {

	/**
	 * Adds the logistic.
	 *
	 * @param log
	 *            the log
	 */
	public void addLogistic(long productId, long storageId, int quantity);

	/**
	 * Checkout product.
	 *
	 * @param logistic
	 *            the logistic
	 * @throws ProductQuantityInsufficientlyException
	 *             the product quantity insufficiently exception
	 */
	public void checkoutProduct(Logistic logistic, long userId)
			throws ProductQuantityInsufficientlyException;

	public void addStorage(String title, int deliveryTime);

	public int getDeliveryTimeById(long storageId)
			throws StorageNotFoundException;

	public int getProductCount(long productId);

	public long getStorageIdByTitle(String title);

	public List<Logistic> getUserLogistic(long productId, int quantity);

	public List<Storage> getStorages();

	public void deleteProductLogistics(long productId);
}
