package ru.lanit.shubanev.dao.abstractdao;

import java.util.List;

import ru.lanit.shubanev.model.access.Permission;
import ru.lanit.shubanev.model.access.Role;
import ru.lanit.shubanev.model.access.RolePermission;
import ru.lanit.shubanev.model.access.UserRole;

/**
 * The Interface AccessDao.
 */
public interface AccessDao {

	public List<Long> getUserRoles(long userId);

	/**
	 * Adds the user role.
	 *
	 * @param userRole
	 *            the user role
	 */
	public void addUserRole(UserRole userRole);

	/**
	 * Adds the role.
	 *
	 * @param role
	 *            the role
	 */
	void addRole(String title);

	/**
	 * Adds the role command.
	 *
	 * @param rolePermission
	 *            the role command
	 */
	public void addRolePermission(RolePermission rolePermission);

	public Role getRoleByTitle(String title);

	public Permission getPermissionByTitle(String title);

	public String getRoleStringById(long roleId);

	public long getPermissionId(String str);

	List<Long> getRolePemissions(long roleId);

	public String getPermissionStringById(long permission);

	void addPermission(String title);

}
