package ru.lanit.shubanev.dao.abstractdao;

import java.util.List;

import ru.lanit.shubanev.exceptions.ProductNotFoundException;
import ru.lanit.shubanev.model.product.Product;
import ru.lanit.shubanev.model.product.Spec;

/**
 * The Interface ProductDAO.
 * 
 * @param <T>
 */
public interface ProductDAO extends PagedDao<Product> {

	/**
	 * Adds the spec.
	 *
	 * @param spec
	 *            the spec
	 */
	public void addSpec(long categoryId, String title);

	/**
	 * Gets the spec.
	 *
	 * @param title
	 *            the title
	 * @return the spec
	 */
	public Spec getSpec(long id);

	/**
	 * Gets the spec category.
	 *
	 * @param specId
	 *            the spec id
	 * @return the spec category
	 */
	public long getSpecCategory(long specId);

	/**
	 * Check id.
	 *
	 * @param isbn
	 *            the isbn
	 * @return the double
	 */
	public double getPriceByCode(String isbn);

	/**
	 * Adds the product.
	 *
	 * @param product
	 *            the product
	 */
	public void addProduct(String title, double price, String desc, String code);

	/**
	 * Gets the category id.
	 *
	 * @param title
	 *            the title
	 * @return the category id
	 */
	public long getCategoryId(String title);

	/**
	 * Adds the category.
	 *
	 * @param category
	 *            the category
	 */
	public void addCategory(String title, long parentId);

	public Product getProductById(long id) throws ProductNotFoundException;

	public long getProductIdByTitle(String title)
			throws ProductNotFoundException;

	public List<Spec> getCategorySpecifications(long categoryId);

	public void addProductSpec(long productId, long specId, String value);

	/**
	 * Categories to string.
	 *
	 * @return the string
	 */
	public String categoriesToString();

	/**
	 * Gets the product by code.
	 *
	 * @param code
	 *            the code
	 * @return the product by code
	 */
	public Product getProductByCode(String code);

	public long getSpecIdByTitle(String title);

	public void deleteProduct(long productId) throws ProductNotFoundException;

}