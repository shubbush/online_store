package ru.lanit.shubanev.dao.abstractdao;

import java.util.List;

import ru.lanit.shubanev.exceptions.UserNotFoundException;
import ru.lanit.shubanev.model.logistic.Logistic;
import ru.lanit.shubanev.model.user.Cart;
import ru.lanit.shubanev.model.user.User;

/**
 * The Interface UserDAO.
 */
public interface UserDAO extends PagedDao<User> {

	/**
	 * Adds the cart.
	 *
	 * @param cart
	 *            the cart
	 */
	public void addCart(Cart cart);

	/**
	 * Adds the user.
	 *
	 * @param login
	 *            the user login
	 * 
	 * @param password
	 *            MD5 hash of the user password
	 */
	public void addUser(String login, String password);

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	// public Collection<User> getUsers();

	public Cart getUserCart(long userId);

	public User getUserByLogin(String login);

	public long getUserIdByLogin(String login);

	public User getUserById(long id) throws UserNotFoundException;

	public User signIn(String login, String password)
			throws UserNotFoundException;

	public void addProductToUserCart(List<Logistic> logistic, long userId);
}
