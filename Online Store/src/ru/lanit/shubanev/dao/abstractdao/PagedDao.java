package ru.lanit.shubanev.dao.abstractdao;

import java.util.Map;

import ru.lanit.shubanev.controller.command.SortFilter;
import ru.lanit.shubanev.exceptions.ItemNotFoundException;
import ru.lanit.shubanev.model.ListItem;

public interface PagedDao<T extends ListItem> {

	T[] getPage(int currentElement, int count, Map<SortFilter, Object> map)
			throws ItemNotFoundException;

	int getItemsCountForRequest(Map<SortFilter, Object> map)
			throws ItemNotFoundException;
}
